%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TUM-Vorlage: Wissenschaftliche Arbeit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Rechteinhaber:
%     Technische Universität München
%     https://www.tum.de
%
% Gestaltung:
%     ediundsepp Gestaltungsgesellschaft, München
%     http://www.ediundsepp.de
%
% Technische Umsetzung:
%     eWorks GmbH, Frankfurt am Main
%     http://www.eworks.de
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{./Ressourcen/Praeambel.tex}
\input{./Ressourcen/Anfang.tex}

\begin{document}

\newcommand{\Titel}{Automatisierte Klassifikation Nuklearkardiologischer Textbefunde mittels Methoden des Machine Learning}
\newcommand{\Untertitel}{Entwicklung und Auswertung von Analyse-Ansätzen}
\newcommand{\EingereichtAmDatum}{30.09.2019}
\newcommand{\ErklaerungUeberschrift}{Anhang I}
\newcommand{\Ort}{München}
\newcommand{\Datum}{30.09.2019}

\input{./Ressourcen/Dokument.tex}

\title{Automatisierte Klassifikation Nuklearkardiologischer Textbefunde mittels Methoden des Machine Learning}
\author{Kilian Schulte, Viviana Sutedjo}
\date{30.09.2019}


\tableofcontents % Inhaltsverzeichnis

\chapter{Kurzbeschreibung}
Die Nuklearmedizinische Klinik und Poliklinik führt pro Jahr um die 1500 Myokardszintigraphien durch. Die Auswertung ist relativ automatisch, aber die Befunde recht unstrukturiert. Das macht es  schwierig, retrospektiv auf diesen gewaltigen Datenschatz zuzugreifen. Für die medizinischen Mitarbeiter bedeutet diese Menge an Befunden und ihre vielfältigen Formulierungen einen großen Verwaltungsaufwand.

Das Projektziel ist es, diese Befunde automatisch zu interpretieren. Dies bedeutet, auf Basis der textlichen Diagnosen zu entscheiden, ob eine normale oder eine abnormale Untersuchung vorliegt. Diese gefilterten Befunde können im nächsten Schritt von einem medizinischen Mitarbeiter untersucht werden, um nachzuverfolgen, ob die empfohlene Therapie oder weiterführende Diagnostik durchgeführt wurde und sich mithin die Untersuchungsergebnisse bestätigt haben. Das Filtern ermöglicht dann Statistiken über die Trefferquote der Ergebnisse und damit eine ständige Verbesserung derselben. Als weiteren Schritt könnten die Einordnungen auf textlicher Basis mit dem jeweiligen Bildmaterial verknüpft werden.

Das Projekt ist im Ganzen eher experimentell angesetzt, es sollen vordergründig verschiedene Herangehensweisen ausprobiert, beurteilt und gegeneinander abgewägt werden, sodass am Ende eine nachhaltige Lösung entwickelt werden kann. Hauptthemen sind hierbei einerseits die sinnvolle Auswertung medizinischer Texte, andererseits die Einspeisung dieser Texte in verschiedene Algorithmen (u.A. Machine Learning) zur Kategorisierung der Befunde.

\chapter{Einleitung}

\section{Einsatzmöglichkeiten und Projektziele}
Das Ziel des Projektes ist es, zu untersuchen, ob und wie eine automatische Beurteilung von textuellen Myokardszintigraphie-Befunden möglich und hilfreich ist. Diese soll Ärzte dabei unterstützen, eindeutigere Befunde zu formulieren, um Missverständnisse zwischen medizinischen Fachkräften zu minimieren.

Weiterhin sollen - im Erfolgsfall - die kategorisierten Befunde als Trainings-Datensatz für Forschungen im Bereich des Machine Learning verwendet werden. Zurzeit sind Machine Learning Ansätze für diese Befunde nicht möglich, da diese eine Grundwahrheit benötigen, nach der sie ihre Auswertungsvariablen trainieren können. Diese Grundwahrheit besteht in diesem Fall aus den Inhalten der Befunde sowie ihrer Kategorisierung. Eine manuelle Kategorisierung ist zu aufwändig. Eine automatische Beurteilung könnte insofern einen ersten Schritt zu der Erstellung eines solchen Datensatzes sein.

Es ist explizit nicht Ziel dieses Projektes, die automatischen Befunde in medizinische Entscheidungen zu involvieren (siehe Limitierungen).

Die Dokumentation beschreibt das Projekt in ihren zwei Hauptteilen: Zunächst wurden experimentell mehrere Ansätze zur Kategorisierung der Befunde analysiert. Daran anschließend wurde ein Auswertungs-Tool entwickelt, welches medizinischen Fachkräften ermöglicht, mit wenigen Eingaben einige Befunde zu kategorisieren und darauf basierend die restlichen Einschätzungen autonom erstellen zu lassen.

\section{Limitierungen}
Das Projekt geht mit folgenden Limitierungen einher:

Befunde haben eine große Variation an Formulierungen. Bedingt durch die individuellen Vorgeschichten des Patienten herrscht eine große Vielfalt an Informationen, die in Befunden festgehalten werden müssen. Die Befunde unterliegen der persönlichen Vorgehensweise des Arztes; so sind gleiche Diagnosen bei unterschiedlichen Ärzten unterschiedlich formuliert oder Aussagen werden verschieden bewertet. So spielen grammatikalische bzw lexikalische Fehler und Abkürzungen in der Formulierung der Befunde ebenfalls eine Rolle und führen gemeinsam zu einer erschwerten eindeutigen Kategorisierung der Befundtexte.

Ein weiteres Problem ist, dass es keine Möglichkeit gibt, die Effektivität der Ansätze zu validieren. Zum einen gibt es keine eindeutige Grundwahrheit, mit der man die Ergebnisse der Ansätze vergleichen könnte. Dies schließt zudem auch eine maschinelle Überprüfung der Ergebnisse aus und somit bleiben nur noch stichprobenartige Überprüfungen durch Menschen. Zum anderen sind die Kategorisierungen selbst subjektiv - was von einem Arzt eher als sehr abnormal eingeschätzt wird, ist bei dem anderen Arzt möglicherweise nur gering abnormal. Die in dieser Arbeit getroffenen Aussagen sind deshalb nur zutreffend unter genannten Gesichtspunkten.

\chapter{Lösungsansatz}
\section{Analyse verschiedener Ansätze}
Die Erstellung der Befundtexte geschieht mithilfe von festgelegten Textbausteinen, die der Arzt in die Textfelder hineinziehen und anschliessend anpassen kann. Dies führt zu der Annahme, dass vorwiegend einheitliche Textstellen (ausgenommen besagter Anpassungen) in den Befundtexten vorliegen.
\subsection{Regex mit fixen Vorgaben}
Basierend auf der Beobachtung, dass tatsächlich die Befunde recht ähnlich strukturiert und formuliert sind, war der erste Ansatz eine naive Regex-Suche: Feste vorgegebene Sätze wurden in ihrer genauen Form in den Texten gesucht und entsprechend ihrer vorgegebenen Kategorie ausgewertet. Die Ergebnisse waren sehr unzutreffend, was damit zu begründen ist, dass die gesuchten Textstellen eine zu starre Struktur hatten und somit viele ähnliche Sätze nur aufgrund von kleineren Abweichungen wie lexikalische Fehler oder Abkürzungen nicht erkannt und damit in die Auswertung einbezogen wurden.

\subsection{Machine Learning Analyse auf Basis von häufigen Indikatoren}
Daraufhin wurden die Texte in mehrere einfache Machine Learning Algorithmen eingespeist, welche zuvor mit einem kleinen manuell erstellen Datensatz trainiert wurden. Wiederum waren die Ergebnisse nicht zufriedenstellend: Zum einen war der Datensatz aufgrund der Größe nicht repräsentativ für den restlichen Datensatz, was dazu führte, dass viele eigentlich relevanten Textstellen ignoriert wurden. Zum anderen waren die Strukturen, nach denen die Algorithmen suchten, nicht genau genug definiert, sodass sie oft unrelevante Textstellen als abnormal bewertete, nur weil sie in einem anderen Kontext als abnormal vorkam.

\subsection{Direkte Analyse auf Basis von bewerteten Sätzen sowie Gesamtbefunden}
Der nächste Ansatz basierte auf der Idee, dass ein Befund mit vielen abnormalen und wenigen guten Textstellen eher abnormal bewertet werden sollte. Dafür wurden die Texte in numerische Form gebracht:

Die manuell kategorisierten Textstellen von vorigem Schritt wurden wiederum in den Befunden gesucht, aber diesmal in numerische Werte (-1, 0 und 1 für normal, möglicherweise abnormal und sicher abnormal) umgewandelt. Diese Werte wurden dann in verschiedene mathematische Formeln gespeist, in dem Versuch, die obige Idee mathematisch darzustellen, sodass die Ergebnisse möglichst zutreffend sind.

Ein Beispiel für eine solche Formel ist die Summe der Wertungen: abnormale Befunde haben im Optimalfall eine hohe Summe (aufgrund oben genannter Umwandlung in -1, 0 und 1), während eher normale Befunde sich im negativen Bereich befinden sollten. Hieraus konnten zusätzliche Erkenntnisse gezogen werden: Beispielsweise gilt, je höher der Betrag, desto abnormaler/normaler war der Befund, also desto sicherer war die Kategorisierung in abnormal/normal. Ein niedriger Betrag schliesst darauf, dass entweder kaum Textstellen gefunden werden konnten, oder annähernd gleich viele gute wie schlechte Textstellen gefunden wurden, sodass eine sichere Kategorisierung nicht möglich ist.

Die daraus erzielten Auswertungen deckten sich in stichprobenartigen Tests zu einem großen Teil mit den tatsächlichen Einschätzungen. Doch Befunde, in denen wenige eingestufte Textstellen vorkamen, waren falsch eingeordnet.

\section{Konzeptionierung der Applikation}
Mit der Erkenntnis, dass ein solch kleiner Wahrheitsdatensatz zu keiner brauchbaren autonomen Kategorisierung führen kann, entwickelte sich die Notwendigkeit, schnell und zutreffend den Datensatz zu vergrößern. Die bisher verwendeten Methoden, nämlich das Markieren von Textstellen in drei Farben in Word und das manuelle Übertragen dieser Kategorien in eine Excel-Tabelle, stellte ein großes Hindernis zu diesem Ziel dar.

Daher wurde ein Tool konzeptioniert, welches die folgenden Ziele vereinigt:

Zum einen soll der Nutzer Textstellen einfach markieren können. Die Markierungen sollen in einem Format ausgegeben werden, das der Kategorisierungs-Algorithmus direkt auswerten kann, um den Schritt des manuellen Übertragens von den Markierungen zu eliminieren. Markierte Sätze sollen im Befundtext angezeigt werden und sollen damit helfen, in Zukunft mehrdeutige Formulierungen zu identifizieren.

Zum anderen soll das Tool bereits markierte Textstellen bei allen Befunden vormarkieren. Der Nutzer kann somit bereits markierte Textstellen überspringen, aber soll trotzdem noch die Möglichkeit haben, diese zu korrigieren, sollte sie in diesem Kontext eine andere Bedeutung haben.

Die markierten Textstellen sollen in einen Algorithmus, basierend auf vorigem Schritt, eingespeist werden können und somit sollen Befunde direkt kategorisiert werden können. Je mehr Textstellen markiert wurden, desto zutreffender wird diese Kategorisierung.

Das Tool soll aus Datenschutzgründen nur offline funktionieren. Es soll zudem auf Desktop-Rechnern (Windows, Linux, Mac) laufen und eine nutzerfreundliche UI haben.

\subsection{Beschreibung der Applikation}

Im folgenden wird die Applikation in ihrem Aufbau und ihrer Funktionalität grob beschrieben. Eine detailliertere Beschreibung der Anwendung sowie eine Benutzungsanleitung liegt dieser als \textbf{README} Datei bei.

\caption{Hauptseite}

\begin{figure}[h]
  \includegraphics[width=8cm]{Bilder/home.png}
  \centering
\end{figure}

Auf der Hauptseite werden zum einen verschiedene Informationen und Auswertungen zu den aktuell geladeden Befunde angezeigt, um eine schnelle Übersicht über den Fortschritt und die Qualität der gemachten Markierungen zu geben. Zum anderen befinden sich auf der linken Seite eine Reihe an Navigationselementen zu den verschiedenen Modulen sowie Werkzeugen zur Verwaltung der Befunde und zum Starten der Auswertung derselben. \\

Der Nutzer kann außerdem aus zwei Auswertungsmodi auswählen: Wenn die \textbf{Live Valuation} aktiviert ist werden alle Befunde ausgewertet sobald eine Änderung bei den Markierungen vorgenommen wird. Andernfalls kann ein Auswertungsdurchlauf manuell über \textbf{Start Valuation} gestartet werden.

Im Dashboard auf der Hauptseite werden folgende Karten und Auswertungen angezeigt:
\begin{itemize}
  \item Report Stats: Informationen über die Anzahl importierter, markierter und ausgewerteter Befunde.
  \item Valuation Results: Tendenzen der drei Auswertungsmetriken (Abnormality, Coverage, Ambiguity) in Form von Boxplots.
  \item Report Trend: Verteilung der Befunde auf den Achsen Ambiguity und Abnormality.
  \item Overview: Ein Radar-Graph zum Vergleich zwischen dem aktuellen Stand der Auswertung und dem idealen Stand (hohe Markierungsdichte, hohe Coverage, niedrige Ambiguity).
  \item Report Distribution: Eine Heatmap mit den Achsen Coverage und Ambiguity. Interessant um zu sehen, in welchem Maße eine höhere Abdeckung zu einer niedrigeren Unsicherheit führt.
\end{itemize}

\caption{Markierungs Modul}

\begin{figure}[h]
  \includegraphics[width=8cm]{Bilder/marking.png}
  \centering
\end{figure}

Das Markierungstool zeigt auf der rechten Seiten den zu markierenden Befund. Unterstrichen sind Textstellen, die bereits von dem Algorithmus erkannt und klassifiziert wurden, da sie beispielsweise bereits in anderen Befunden markiert wurden. Hierbei zeigt Grün normale, Rot abnormale und Orange leicht abnormale Markierungen an. Am linken Rand der Seite kann der Nutzer aus diesen Farben auswählen, um weitere Textstellen im vorliegenden Befund entsprechend zu markieren. Die graue Farbe ist dazu da, Textstellen als neutral zu markieren. Dies ist der Fall bei Wörtern, die in keinem Fall als normal/abnormal bewertet werden sollen, sowie bei Textstellen, die unterstrichen sind, aber in diesem Kontext keine Bedeutung haben sollen. Die graue Spalte ermöglicht dem Benutzer einige Einstellungen zu dem Tool vorzunehmen und zeigt unten zudem die Auswertung des aktuellen Befundes an.

\caption{Verfassungs Modul}

\begin{figure}[h]
  \includegraphics[width=8cm]{Bilder/writing.png}
  \centering
\end{figure}

Das Verfassungstool ermöglicht es dem Benutzer, einen neuen Befund zu verfassen und zu speichern. Wie bei dem Markierungstool werden auch hier bereits klassifizierte Textstellen in der jeweiligen Farbe unterstrichen sowie die Auswertungsmetriken angezeigt. Dies ermöglicht ein direktes Feedback an den Nutzer wie der Algorithmus den gerade verfassten Befund einschätzt.

\caption{Extraktions Modul}

\begin{figure}[h]
  \includegraphics[width=8cm]{Bilder/extracting.png}
  \centering
\end{figure}

Mithilfe dieses Tools können zusätzlich zu den Markierungen weitere Daten aus den Befunden extrahiert werden und zur Auswertung hinzugezogen werden. So können beispielsweise feste numerische Werte als normal/abnormal festgelegt werden und entsprechend ihrer Aussagekraft gewichtet werden. Das Tool verwendet reguläre Ausdrücke, welche zwar eine spezielle Kenntnis des Nutzers erfordern, allerdings sehr mächtig in ihrer Verwendung sind. Um nicht in jedem Markierungsprozess erneut erstellt werden zu müssen lässt sich die gesamte Konfiguration des Tools exportieren sowie importieren.

\caption{Report Liste}

\begin{figure}[h]
  \includegraphics[width=8cm]{Bilder/reports.png}
  \centering
\end{figure}

Eine Liste aller Befunde. Diese können oben nach verschiedenen Kriterien sortiert werden. Der farbige Punkt auf der linken Seite zeigt die Auswertungsmetriken wie folgt: Die Farbe des Punktes zeigt die Abnormalität des Befundes (von Rot nach Grün), und die Größe zeigt die Coverage an, d.h. wie viele Textstellen vom Befund erkannt wurden. Auf der rechten Seite befindet sich außerdem ein Indikator zu den Markierungen des entsprechenden Befundes.

\subsection{Berechnung der automatischen Beurteilung}

Der Algorithmus besteht aus zwei Phasen, die nacheinander ausgeführt werden.

In der ersten Phase werden alle Markierungen analysiert und aggregiert. Dabei werden folgende Transformatinen angewendet: Markierungen, die mehrere Wörter beinhalten werden in alle möglichen Wortketten aufgesplittet, die aus ein bis x Wörtern bestehen. Im weiteren Schritt werden alle Wortketten gelöscht, die mit einer neutralen Markierung überein stimmen. Im letzten Schritt werden nur diejenigen Wortketten behalten, zu denen es eindeutige Markierungen gibt, d.h. nur Markierungen einer Farbe. Wortketten die beispielsweise einmal in Grün und einmal in Rot markiert wurden werden gelöscht.

In der zweiten Phase werden die generierten Wortketten auf die Befunde abgeglichen. Bei zwei Treffern auf den selben Textabschnitt wird die längere Wortkette verwendet, da diese mit dem jeweiligen Kontext eines Befundes besser übereinstimmt. Aus den Treffern wird letztendlich der Beurteilungswert errechnet. Dabei werden für jede Wortkette folgende Kennwerte berücksichtigt: Die Farbe der Markierung, die Anzahl der Markierung sowie die Länge der Wortkette.

Als Beurteilung werden drei Metriken errechnet: Abnormality, Ambiguity (Eindeutigkeit) und Coverage (Abdeckung). Diese bilden sich aus den Kennwerten wie folgt:

Abnormality ist bedingt durch die am häufigsten auftretende Farbe in den Markierungen der Wortketten. Dazu wird ein Durschnittswert gebildet.

Ambiguity is bedingt durch die Farbverteilung in den Markierungen der Wortketten sowie durch die Lände der Wortketten. Viele unterschiedliche Farben oder kurze Wortketten weisen auf eine niedrige Eindeutigkeit hin, wohingegen gleiche Farben oder lange Wortketten auf eine hohe Eindeutigkeit hinweisen.

Coverage ist bedingt durch die Anzahl und Länge der Wortketten. Viele oder lange Wortketten erhöhen die Abdeckung, wobei wenige oder kürzere Wortketten die Abdeckung verringern.

\section{Fazit}
Zusammenfassend kann festgehalten werden, dass die Applikation bereits gute Einschätzungen liefert, dazu jedoch anfangs noch menschliche Eingaben benötigt.

Da die Kategorisierung der markierten Textstellen über die Jahre nicht verfallen, können bereits vorgenommene Markierungen auf den gesamten Datensatz aller Jahre verwendet werden, sofern diese dieselben Formate besitzen. Somit reduziert das entwickelte Programm die zur Analyse benötigte Zeit und kann schlecht bewertete Befunde effizient identifizieren. Das automatische Bewerten von Befunden ist ein vielversprechender Ansatz, der auch in anderen medizinischen Bereichen aufgegriffen werden könnte.

Die Einschätzung der Effektivität dieses Ansatzes ist wie bereits erwähnt schwierig, allerdings stellt das Tool einen ersten Schritt zu einer automatischen Kategorisierung dar und ist bereits in seiner aktuellen Form in vielen Fällen aussagekräftig.

\chapter{Zukünftige Forschung}
Wie bereits anfangs angesprochen, ist ein interessanter Ansatzpunkt das Trainieren eines Machine Learning Algorithmus zur Auswertung von oder mit reinen Bilddaten. Einerseits geben die Bilddaten, falls es bereits ausgewertete gibt, dem Befund-Datensatz einen weiteren Faktor, der zur Kategorisierung der Befunde zur Hilfe genommen werden kann. Andersherum könnten die Kategorisierungen der Texte bei der automatischen Beurteilung von Bilddaten helfen: Wenn zu einem Bild die Auswertung aufgrund des zugehörigen Befundtextes bekannt ist, kann man mit diesem Paar Machine Learning Algorithmen für die Kategorisierung von Bilddaten trainieren.

Das Tool selbst kann auch optimiert werden, beispielsweise könnten mehr Analyse-Tools hinzugefügt oder die UI (insbesondere bei der Erstellung von eigenen numerischen Filtern) erweitert werden.

\end{document}
