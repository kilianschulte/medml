# MedTool

<style>
@import url('./node_modules/@mdi/font/css/materialdesignicons.min.css');

.underline {
  position: relative;
}
.underline::after {
    content: "";
    display: block;
    position: absolute;
    height: 2px;
    bottom: -3px;
    left: 0;
    right: 0;
    background: limegreen;
}

.button {
  padding: .2em .5em;
  border: 2px solid limegreen;
  border-radius: 5px;
}

</style>

A tool to create datasets of medical reports for supervised learning by marking words and
sentences regarding their medical assessment.

### Who this is for

This tool is for anyone who wants to create training datasets for supervised learning based
on textual inputs. This tool was developed and tested with a medical background but is not
limited to that. Although if you want to use the tool in another context you may want to change
some wording.

### How does it work

In the process of **valuation** each report is mapped to a percentual value called **Abnormality**.
This value shall indicate the degree of abnormal findings throughout the report. Therefore 0% suggests
a healthy patient and 100% suggests an ill patient.

The valuation of a report is calculated by mapping phrases with known value to the text in question. When
two phrases map to the same section of the text, the phrase with the longer context will be chosen. The
valuation phrases are obtained from the marking of reports in a color being one of green for 'normal',
yellow for 'probably abnormal', and red for 'abnormal'.

Besides Abnormality, two additional metrics are calculated to give a better assessment of the report.
**Coverage** indicates the coverage of the matched phrases regarding the length of the reports texts. A good
coverage usually points to a high meaningfullness of the valuation value.
**Ambiguity** represents the uniformity of matched phrases. E.g if nearly all matched phrases have an equal value (marked color), the ambiguity is low. This again gives an indication of the meaningfullness of the valuation value.

Therefore the desired optimal valuation of a report is a high coverage and a low ambiguity.

## Quick Start

When you first open the application you need to load a reports file through the **File** menu. This must be a csv file with the following columns:

**| Patient Id | Documentation | Findings | Anamnesis | Assessment | Technique |**

Next navigate to the Markings Module through the **Modules** menu. Choose a marking color by clicking on one of the three **colored marker icons** <i class="mdi mdi-marker" style="color: limegreen"></i> on the left and make a selection of the text on the right.

Now you see two things:

1. The <span style="background: limegreen">background of your selection</span> will be shown in the color you selected. This indicates your marking.
2. The text you selected will be <span class="underline">underlined</span> in the same color. This indicates that the algorithm has learned and identified the text as the correct valuation.

If you selected a word or phrase that occurs at some other place in the report, it now should be **underlined as well**.

Make sure to read the section about [How to mark](#howtomark) to get some important tips and tricks about marking your reports so that you get the best results.

When you have finished marking the current open report, you can traverse to the next one by clicking on <span class="button">NEXT</span>

## Build from source

### Requirements

In order to build the application from source you need **NodeJS** installed, which comes with its packet manager **npm**.

### Building

To install all necessary dependencies and build an executable for your current operating system run:

```bash
npm install
npm run electron:build
```

## Usage

### Application Overview

This section is meant to give an overview over all elements and modules of the application.

On every page you have the **App Bar** at the top of the window. Through this you can

- Go back to the home screen.
- Search for a specific report or its content.
- Navigate to any module of the application.
- View the marking history.

#### Home screen

The home screen shows you a grid of diagrams visualizing different metrics of the reports. Furthermore it contains a sidebar where you can

- Navigate to one of the modules
- Control the valuation of reports
- Load and import files

![](./src/assets/home.png)

#### Modules

There are 4 modules to the application providing different means of **editing** and **valuating** the reports. These are:

1. [Marking module](#marking)
2. [Writing module](#writing)
3. [Extraction module](#extraction)
4. [Reports module](#reports)

#### Hints

Throughout the Application little gray **information icons** <i class="mdi mdi-information"></i> can be found. By hovering these you get more information to specific parts and functionality of the application.

### Loading Data

Before you can use the application you have to load some data. This can be done over the **File** menu at the top or through the sidebar on the home screen.

At first it is required to load a list of **reports** that shall be marked. This must be a csv file with the following columns:

**| Patient Id | Documentation | Findings | Anamnesis | Assessment | Technique |**

After this you are ready to go. As an **optional step** you can import already existing **markings** through the *Import > Markings* menu item. This can give you a headstart on your marking. However you cannot edit imported markings in contrast to markings created through the application.

---

When you decided to import a list of existing markings it might be a good idea to [valuate your reports](#valuate) for the first time.

After importing you can view a list of all reports at the [Reports module](#reports).

### <a id="valuate"></a>Valuating Reports

In order to valuate all reports aidt once navigate to the home screen and click <span class="button" style="border-color: #2d82f4">Start Valuation</span>.

When marking a report, the valuation is done automatically for the viewed report after each change, so you don't have to re-valuate all reports while editing.

Also there is an option labeled <span class="button" style="border-color: #2d82f4">Live Valuation</span>, which will turn on automatic valuation for all reports at once. However this might get cpu extensive, depending on your system.

---

When valuating a report the following 3 metrics are calculated:

#### 1. Abnormality

**Abnormality** is the main metric for assessing a report. This value shall indicate the degree of abnormal findings throughout the report. Therefore 0% suggests
a healthy patient and 100% suggests an ill patient.

#### 2. Coverage

**Coverage** indicates the coverage of the matched phrases regarding the length of the reports texts.

A good coverage usually points to a high meaningfullness of the valuation value. Therefore a high coverage is desired.

#### 3. Ambiguity

**Ambiguity** represents the uniformity of matched phrases. E.g if nearly all matched phrases have an equal value (marked color), the ambiguity is low.

This again gives an indication of the meaningfullness of the valuation value. Therefore a low ambiguity is desired.

## Collaboration

This application does not support online collaboration. However if you plan to work with multiple people on the same set of reports you can do so by **sharing your markings** through exporting them. When importing multiple marking files they are **merged** and you can select wether to overwrite the existing markings or ignore duplicates. Keep in mind that you cannot edit imported markings inside the application. However you can of course edit the csv files directly.

If you want to save your valuation results go to *File > Save Results*, which will give you a csv file containing all reports together with their [three valuation metrics](#valuate).

## Modules

### <a id="marking"></a> Marking Module

![](./src/assets/marking.png)

#### Elements on the page

The main content is shown as a card on the right. There you can select the text inside the boxes to create a marking.

To the left of the main content is a sidebar with **options** and **navigation**:

1. Selection of the [Traverse order](#traverse)
2. Selection of the shown report columns
3. [x] Checkbox to underline valuated markings
4. Button to <span class="button">Reset</span> all markings
5. Button to go to the <span class="button">Next</span> report

At the bottom of the sidebar some metrics regarding the [report valuation](#valuate) are shown.

At the very left sits the **Toolbar** containing:

- 3 colored [marking tools](#howtomark) <i class="mdi mdi-marker" style="color: limegreen"></i><i class="mdi mdi-marker" style="color: gold"></i><i class="mdi mdi-marker" style="color: crimson"></i>
- 1 grey [marking tool](#howtomark) <i class="mdi mdi-marker" style="color: grey"></i>
- an eraser to remove markings <i class="mdi mdi-eraser" style="color: black"></i>
- a magnifying glass to [inspect valuations](#inspect) <i class="mdi mdi-magnify" style="color: black"></i>

#### <a id="howtomark"></a> How to mark

When you want to mark some text you first has to decide which of the three colors <i class="mdi mdi-marker" style="color: limegreen"></i><i class="mdi mdi-marker" style="color: gold"></i><i class="mdi mdi-marker" style="color: crimson"></i> to choose. The colors represent the degree of abnormality that is represented by the marked text, in detail:

- <span style="color: limegreen">Green</span>: **Normal**, hints to a **healthy** condition.
- <span style="color: crimson">Red</span>: **Abnormal**, hints to an **ill** condition.
- <span style="color: gold">Yellow</span>: **Probably abnormal**, hints to an **ill** condition, but is not as determinant as red. (It is calculated as an 80% chance of abnormality.)

**Hence <span style="color: gold">Yellow</span> does not lay in the middle between <span style="color: limegreen">Green</span> and <span style="color: crimson">Red</span> and does not represent a neutral state!**

Instead any neutral text should by default **not** be marked at all.

---

These three colors should be sufficient in most cases. However be aware that you can **influence the valuation** of phrases in a more precise manner by **balancing the frequency** of your markings of a repeating phrase.

For example if you want to indicate a phrase being **Probably normal** with a similar interpretation than a <span style="color: gold">Yellow</span> marking, you can do so by marking only 4 out of 5 occurrences of the phrase accross the reports. This will naturally be recognised as that you are not 100% sure about this particular phrase.

---

##### Neutral markings

Besides the three colored markers there is a forth marker <i class="mdi mdi-marker" style="color: grey" ></i> in grey. This marking indicates a **neutral meaning** of the selected text.

As stated above a neutral meaning is assigned to the text by default, therefore marking <span style="background: lightgrey">text in grey</span> does not have an effect if the selected text **does not have a valuation** already.

Instead the grey marker <i class="mdi mdi-marker" style="color: grey" ></i> should be used to **correct valuations**, that are unintended. This mostly happens to single words that, when used for example in a <span style="background: crimson">abnormal marked phrase</span>, are also **valuated as abnormal** although the single word for itself **does not carry any indication towards a valuation**.

Different to the colored markings, the grey marking can be used **on top** of an already existing marking.

---

##### Best practices

You will receive the most satisfying results when sticking to the following principles:

- **Do not** mark multiple sentences at once! If you want to mark subsequent sentences you can split them by **excluding the punctuation mark** from the marking.
- **Do mark** as much context as is being useful. Avoid marking single words wherever possible.
- **Do not** select a metric that can convey different meanings on different sizes. Or simply do not select any numbers. Instead handle numeric valuations with the [Extraction module](#extraction).

#### Correct markings

You have several possibilities to correct your markings. You can either use the **eraser tool** <i class="mdi mdi-eraser"></i> to erase specific selections or press the <span class="button" style="border-color: #444">Reset</span> button to clear all markings of the current report.

Both these options are **tracked by the markings history** <i class="mdi mdi-history"></i>.

![](./src/assets/history.png)

You find the **marking history** <i class="mdi mdi-history"></i> in the app bar on the very top of the application window. When clicking on the icon you open a popup displaying **all the changes** you made **grouped by the report**. For each report you have two actions to use:

- **Undo One** <i class="mdi mdi-undo"></i> reverts the last change of the report.
- **Undo All** <i class="mdi mdi-replay"></i> reverts all changes of the report.

#### Valuation of the report

The live-updated valuation of the currently opened report can be viewed at the bottom of the options panel.

![](./src/assets/stats.png)

You see the percentual values for the three metrices as well as some additional lines above (*Auswurffraktion: ...*). These lines correspond to the results retrieved through the [extraction module](#extraction). It shows the calculated abnormality in percent as well as the weight of the extraction.

When there are some extractions made from the report you will have two additional values in light gray text next to the abnormality and ambiguity metric. These are the values **before** the extractions were calculated in. It might help you to see the effect an extraction has on the report valuation. *(In the picture above, the 'Auswurffraktion' extraction improves both the abnormality and ambiguity.)*

#### <a id="inspect"></a> Inspect a valuation

The last tool - the magnifying glass <i class="mdi mdi-magnify" style="color: black"></i> - enables you to **inspect a valuation** of a word or sentence. When selecting <span class="underline">underlined text</span> it shows a popup containing two sections containing the **original markings** from which the shown valuation was derived.

1. **Markings used for valuation** are a filtered list of markings that were used to calculate the valuation of the selected text.
2. **All found markings** are a list of all markings that relate to the selected text.

Inside **All markings** some items may have a **link icon** <i class="mdi mdi-link"></i>. These are markings from the current loaded reports. You can click on these items to get to the original report. All other entries are imported markings and cannot be linked to any report.

#### <a id="traverse"></a> Traverse reports

You can traverse the list of reports by clicking on the <span class="button">Next</span> button. The order of traversal can be configured through the **Order By** option on top of the sidebar. You can choose between the following options:

- **Default** sorts the reports by the order they were loaded.
- **Unmarked first** sorts the reports by the amount of their markings.
- **Most ambiguous first** sorts the reports by ambiguity.
- **Least covered first** sorts the reports by coverage.

### <a id="writing"></a> Writing module

![](./src/assets/writing.png)

#### Elements on the page

This module has a similar structure than the marking module. On the right, the main content is shown as a card. For each report column there is a textual input field, as well as a documentation field at the top.

The sidebar on the left contains the following **options** and **navigation elements**:

1. Input for the **Report Id**. This will be generated automatically if left empty.
2. Input for the **Patient Id**
3. [x] Checkbox to underline valuated markings
4. Button to <span class="button">Clear</span> all written content
5. Button to <span class="button">Save</span> the report

Yet again at the bottom of the sidebar some metrics regarding the [report valuation](#valuate) are shown.

#### When to use

There are multiple occasions when you might want to use this module:

- As a author of reports (such as a doctor) you can do a self check on your new reports to make sure that the report is valuated by the algorithm as intended. (*Did i write something unclear or misleading?*)
- As a marker of reports (such as a student) you can evaluate the quality of the algorithm to make sure that the algorithm valuates the report as intended. (*Did the algorithm underline the correct words?*)

#### Custom reports

When saving a custom report it gets added to the reports list and handled just as any other report. However in the [reports module](#reports) you will see a **delete icon** <i class="mdi mdi-delete"></i> next to the report.

### <a id="reports"></a> Reports module

![](./src/assets/reports.png)

The reports module provides a list of all loaded reports. By clicking on an entry you can open the [marking module](#marking) for the specific report.

At the left of each entry there is a **colored circle**, which indicates the valuation of the report:

- The color indicates the **abnormality** (<span style="color: crimson">red</span> to <span style="color: limegreen">green</span>)
- The size indicates the **coverage**

At the right of each entry is a **marking indicator**, which indicates the amount and type of marking done in the report.

#### Sorting

The list is sortable through the controls at the top right. You can select the metric to sort by as well as the direction of sorting.

The options are:

- **Default**: Does not sort the reports
- **Abnormality**
- **Coverage**
- **Ambiguity**
- **Marking**: Sorts by the amount of marking

#### Searching

When using the search bar from the report module, instead opening the search popup, the results will be shown directly inside the report list.

![](./src/assets/report-search.png)

### <a id="extraction"></a> Extraction module

Using this tool, further data can be extracted from the reports in addition to the markings, which then can be used for the valuation. For example, fixed numerical values can be defined as normal/abnormal and weighted according to their meaningfulness.

#### Elements on the page

On the left is the main configuration panel containing all extractions. At the bottom you can create a new extraction using the <span class="button"><i class="mdi mdi-plus"></i> New Extraction</span> button.

Each extractions has a number of input for the **search expression** and the **extraction functions**. For each function you can either choose from an existing one or write a custom one.
Additionally you can specify the **columns** to apply the extraction to and the **weight** of the extraction.

By clicking the **Find Results** <i class="mdi mdi-file-find"></i> icon, you can test the extraction and inspect the results on the right.

#### How do extractions work

An extraction consists of a **regular expression** as well as three functions: The **Map, Aggregate and Valuation** function. All three functions work together to calculate a single value from 0 to 1 about the abnormality of the extracted information.

It can be assigned to one or more **columns** and has a **weight** which defines how much the extraction influences the report valuation.

#### The search expression

The search expression is a standard regular expression. It must define **capture groups** which contents will be provided to the **mapping function**. Numbers will automatically be parsed.

#### The functions

The user can choose one of the provided functions or write a custom one. The custom functions are written in javascript and can take one of three forms:

**1. A single line expression** like <code>1 + 2</code> or <code>"abcde".charAt(x)</code>

**2. A self invoking function** in the form <code>( (param1, param2) => { ...; return x; } ) (1, 2)</code>

**3. A block statement** in the form <code>{ let x = 1; let y = x + 2; y }</code>

#### The Map function

The **Map function** gets called for each match of the search expression. Note that there can be more than one matches in a single report.

The function is used to transform each matches by performing simple operations.

#### The Aggregate function

The **Aggregate function** gets called for each report once. Its purpose is to aggregate all matches of a single report into a single value.

It gets executed onto the transformed matches returned from the Map function

The written expression will be used as a **reducer** on the array of matches.

#### The Valuation function

The **Valuation function** transforms the aggregated value into a valuation value.

It must return a value **between 0 and 1** with 0 being *'normal'* and 1 being *'abnormal'*.
