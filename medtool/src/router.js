import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import Reports from "@/views/Reports.vue"
import Marking from "@/views/Marking.vue"
import Writing from "@/views/Writing.vue"
import Extraction from "@/views/Extraction.vue"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/marking/:reportId?',
      name: 'marking',
      component: Marking
    },
    {
      path: '/reports',
      name: 'reports',
      component: Reports
    },
    {
      path: '/writing',
      name: 'writing',
      component: Writing
    },
    {
      path: '/extraction/:reportId?',
      name: 'extraction',
      component: Extraction
    }
  ]
})
