
import Vue from "vue";

const { dialog } = require('electron').remote;

import {getReportId, getMarkings, runWorkers, setQueue, getQueue, openCSV, progress} from "@/helpers.js"

// The worker thread for computing extractions
import ExtractionWorker from 'worker-loader!@/workers/extractionWorker.js';
let exworker = new ExtractionWorker();

// The worker thread for transforming markings into valuations
import MarkingsWorker from 'worker-loader!@/workers/markingsWorker.js';
let markworker = new MarkingsWorker();

import _ from "lodash";
import fs from "fs";

export default {
  /**
   * Schedules all reports for valuation
   * @param  {Object} state    The vuex state
   * @param  {Function} commit   The vuex commit function
   * @param  {Function} dispatch The vuex dispatch function
   */
  scheduleAll({state, commit, dispatch}) {
    console.time("scheduleAll"); //eslint-disable-line
    progress.reset();
    // schedule all reports and register a callback function
    dispatch("schedule", {entries: state.reports.map(r => ({report: r, prio: 0})), onFinish: () => {
      console.timeEnd("scheduleAll"); //eslint-disable-line
      commit("setAllValuated", true);
      // display notification
      Vue.notify({
        group: 'alerts',
        title: 'Finished Valuating',
        text: `Successfully finished valuating ${state.reports.length} reports. You may now save the results.`,
        'animation-type': 'velocity',
        position: 'top right',
        type: 'success',
        duration: 5000
      })
    }})
  },
  /**
   * Schedule reports for valuation
   * @param  {Object} state      The vuex state
   * @param  {Function} commit   The vuex commit function
   * @param  {Function} dispatch The vuex dispatch function
   * @param  {Array | Object} p           Array of reports or single report
   */
  schedule({state, commit, dispatch}, p) {

    let entries, cb;
    if (p instanceof Array) {
      entries = p;
    } else {
      entries = p.entries;
      cb = {onFinish: p.onFinish};
    }

    // remove reports from the queue scheduled twice
    let toRemove = [], toAdd = [];
    let queue = getQueue();
    entries.forEach(e => {
      let i = queue.findIndex(q => q.report.id == e.report.id);
      if (i !== -1) {
        if (queue[i].prio > e.prio) {
          e.prio = queue[i].prio;
        }
        toRemove.push(e.report.id);
      }
      toAdd.push(e);
    })
    queue = queue.filter((e) => toRemove.indexOf(e.report.id) == -1);

    // update the queue
    setQueue([...toAdd, ...queue]);
    // start the webworkers
    runWorkers({state, commit, dispatch}, cb);
  },
  /**
   * Compute the valuations from the markings
   * @param  {Object} state    The vuex state
   * @param  {Function} commit The vuex commit function
   */
  computeValuations({state, commit}) {
    // The neutral markings
    let neutrals = [
      ...state.importedMarkings.filter(v => v.value == "neutral"),
      ..._.flatten(state.reports.map(r => getMarkings(r, "mn").map(v => ({text: v, id: r.id}))))
    ]
    // The markings to valuate
    let markings = [
      ...state.importedMarkings.filter(v => v.value !== "neutral"),
      ..._.flatten(state.reports.map(r => getMarkings(r, "mg").map(v => ({text: v, id: r.id, value: "normal"})))),
      ..._.flatten(state.reports.map(r => getMarkings(r, "my").map(v => ({text: v, id: r.id, value: "preabnormal"})))),
      ..._.flatten(state.reports.map(r => getMarkings(r, "mr").map(v => ({text: v, id: r.id, value: "abnormal"}))))
    ]

    markworker.terminate();
    markworker = new MarkingsWorker();
    markworker.postMessage({markings, neutrals});

    return new Promise((resolve) => {
      markworker.onmessage = ({data}) => {
        commit("updateValuations", data)
        console.log("VALUATION COMPUTED"); //eslint-disable-line
        resolve();
      }
    })
  },
  /**
   * Compute the extractions for all reports
   * @param  {Object} state    The vuex state
   * @param  {Function} commit The vuex commit function
   */
  computeExtractions({state, commit}) {
    exworker.postMessage({
      extractions: state.extractions,
      reports: state.reports.map(r => ({id: r.id, source: r.source}))
    });
    exworker.onmessage = ({data}) => {
      commit("updateExtractions", data);
      // console.log(data);
      console.log("EXTRACTIONS COMPUTED"); //eslint-disable-line
    }
  },
  /**
   * Open new reports and add to the database
   * @param  {Object} state      The vuex state
   * @param  {Function} commit   The vuex commit function
   */
  openReports({state, commit}) {
    openCSV("Open Reports")
      .then(json => {
        if (!json) return;

        // get duplicates
        let duplicates = state.reports.filter(r => json.find(j => getReportId(j) == r.id)).length;
        let count = json.length;

        json = _.uniqBy(json, getReportId);

        let notify = (num) => Vue.notify({
          group: 'alerts',
          title: 'Reports loaded',
          text: `Successfully loaded ${num} reports.`,
          'animation-type': 'velocity',
          position: 'top right',
          type: 'success',
          duration: 5000
        })

        if (duplicates > 0) {
          // ask if new reports should overwrite existing reports
          dialog.showMessageBox({
            type: "question", buttons: ["Ignore", "Overwrite", "Cancel"],
            defaultId: 2, cancelId: 2,
            title: "Duplicates detected.",
            message: `The selected file contains ${duplicates} duplicates of already loaded reports (Reports in file: ${count}).\nDo you want to override the existing reports with the new ones or ignore all duplicates?`
          }, (response) => {
            if (response == 1) {
              commit("insertReports", {reports: json, overwrite: true});
              notify(count);
            } else if (response == 0) {
              commit("insertReports", {reports: json, overwrite: false});
              notify(count-duplicates);
            }
          })
        } else {
          commit("insertReports", {reports: json, overwrite: false});
          notify(count);
        }
      })
      .catch(err => {
        console.error("Error on open csv",err); //eslint-disable-line
      })
  },
  /**
   * Save report valuation results with testing for unvaluated reports
   * @param  {Object} state      The vuex state
   * @param  {Function} dispatch The vuex dispatch function
   */
  async saveResults({state, dispatch}) {
    if (state.reports.some(r => !r.result)) {
      let unvaluated = state.reports.filter(r => !r.result).length;
      // ask for valuation of unvaluated reports
      dialog.showMessageBox({
        type: "question", buttons: ["Continue", "Valuate", "Cancel"],
        defaultId: 2, cancelId: 2,
        title: "Unvaluated Reports.",
        message: `There are still ${unvaluated} unvaluated reports. Do you want to continue or start valuating now?`
      }, (response) => {
        if (response == 1) {
          dispatch("schedule", state.reports.filter(r => !r.result).map(r => ({report: r, prio: 0})))
        } else if (response == 0) {
          dispatch("saveAll")
        }
      })
    } else {
      dispatch("saveAll")
    }
  },
  /**
   * Save report valuation results
   * @param  {[type]} state [description]
   * @return {[type]}       [description]
   */
  saveAll({state}) {
    // prepare results table
    let reports = state.reports.filter(r => r.result).map(r => [
      r.source[0], r.result.abnormality.toFixed(3), r.result.coverage.toFixed(3), r.result.ambiguity.toFixed(3)
    ].join(", ")).join("\n");
    // save results
    dialog.showSaveDialog(
      {title: "Save Reports", filters: [{name: "Table", extensions: ["csv"]}], defaultPath: "reports.csv"},
      (file) => { if (file) fs.writeFile(file, reports, () => {}); }
    );
  },
  importMarkings({state, commit}) {
    openCSV("Import Markings")
      .then(json => {

        let markings = _.flatten(json.map(j => [
          {value: "normal", text: j[0]}, {value: "preabnormal", text: j[1]}, {value: "abnormal", text: j[2]}, {value: "neutral", text: j[3]}
        ])).filter(v => typeof v.text === 'string' && v.text.length > 0)

        // get duplicates
        let duplicates = state.importedMarkings.filter(im => markings.find(v => v.text == im.text)).length;
        let count = markings.length;

        if (duplicates > 0) {
          // ask if new markings should overwrite existing reports
          dialog.showMessageBox({
            type: "question", buttons: ["Ignore", "Overwrite", "Cancel"],
            defaultId: 2, cancelId: 2,
            title: "Duplicates detected.",
            message: `The selected file contains ${duplicates} duplicates of already loaded markings (Markings in file: ${count}).\nDo you want to override the existing markings with the new ones or ignore all duplicates?`
          }, (response) => {
            if (response == 1) {
              commit("insertMarkings", {markings, overwrite: true});
            } else if (response == 0) {
              commit("insertMarkings", {markings, overwrite: false});
            }
          })
        } else {
          commit("insertMarkings", {markings, overwrite: false});
        }
      })
      .catch(err => {
        console.error("Error on open csv",err); //eslint-disable-line
      })
  },
  /**
   * Saves all markings to a file
   * @param  {Object} state The vuex state
   */
  exportMarkings({state}) {
    let markings = [
      ...state.importedMarkings,
      ..._.flatten(state.reports.map(r => getMarkings(r, "mg"))).map(v => ({value: "normal", text: v})),
      ..._.flatten(state.reports.map(r => getMarkings(r, "my"))).map(v => ({value: "preabnormal", text: v})),
      ..._.flatten(state.reports.map(r => getMarkings(r, "mr"))).map(v => ({value: "abnormal", text: v})),
      ..._.flatten(state.reports.map(r => getMarkings(r, "mn"))).map(v => ({value: "neutral", text: v}))
    ]

    let f = (k) => markings.filter(v => v.value == k).map(v => `"${v.text}"`);
    let a = f("normal"), b = f("preabnormal"), c = f("abnormal"), d = f("neutral");

    let count = Math.max(a.length, b.length, c.length, d.length);
    let table = new Array(count).fill(0).map((_, i) => [a[i], b[i], c[i], d[i]].join(",")).join("\n");

    dialog.showSaveDialog(
      {title: "Export Markings", filters: [{name: "Table", extensions: ["csv"]}], defaultPath: "markings.csv"},
      (file) => { if (file) fs.writeFile(file, table, () => {}); }
    );
  },
  /**
   * Load extractions from a file
   * @param  {Function} commit   The vuex commit function
   */
  importExtractions({commit}) {
    openCSV("Import Extractions")
      .then(json => {
        let extractions = json.map(e => ({
          name: e[0],
          searchExp: decodeURI(e[1]),
          map: {id: e[2], code: decodeURI(e[3])},
          agg: {id: e[4], code: decodeURI(e[5]), init: decodeURI(e[6])},
          val: {id: e[7], code: decodeURI(e[8])},
          weight: parseInt(e[9]),
          columns: e[10].split(";").map(c => parseInt(c)),
          expand: false
        }));
        commit("insertExtractions", extractions);
      })
      .catch(err => {
        console.error("Error on open csv",err); //eslint-disable-line
      })
  },
  /**
   * Save extractions to a file
   * @param  {Object} state The vuex state
   */
  exportExtractions({state}) {
    let extractions = state.extractions.map(e => [
      e.name, '"'+encodeURI(e.searchExp)+'"',
      e.map.id, '"'+encodeURI(e.map.code)+'"',
      e.agg.id, '"'+encodeURI(e.agg.code)+'"', '"'+encodeURI(e.agg.init)+'"',
      e.val.id, '"'+encodeURI(e.val.code)+'"',
      e.weight, e.columns.join(";")
    ].join(",")).join("\n");

    dialog.showSaveDialog(
      {title: "Export Extractions", filters: [{name: "Table", extensions: ["csv"]}], defaultPath: "extractions.csv"},
      (file) => { if (file) fs.writeFile(file, extractions, () => {}); }
    );
  }
}
