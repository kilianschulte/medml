
import _ from "lodash"
import { createPersistedState } from "vuex-electron"

export default [
  createPersistedState(),
  (store) => { // History plugin
    let takeSnapshot = (state) => _.cloneDeep(state.reports.map(r => ({id: r.id, marks: r.marks})));
    let snap = takeSnapshot(store.state)
    store.subscribe(({type, payload}, state) => {
      // add item with previous marking state to history
      if (type == "eraseMarks" || type == "updateMarks") {
        let report = state.reports.find(r => r.id == payload.id);
        let prevReport = snap.find(r => r.id == payload.id);
        if (!report || !prevReport) return;

        store.commit("pushHistory", {
          type, payload, id: report.id, source: report.source,
          before: _.cloneDeep(prevReport.marks), after: _.cloneDeep(report.marks)
        })
      } else if (type == "resetMarks") {
        let report = state.reports.find(r => r.id == payload);
        let prevReport = snap.find(r => r.id == payload);
        if (!report || !prevReport) return;

        store.commit("pushHistory", {
          type, payload, id: report.id, source: report.source,
          before: _.cloneDeep(prevReport.marks), after: _.cloneDeep(report.marks)
        })
      }
      snap = takeSnapshot(state);
    })
  },
  (store) => { // valuation watch plugin
    store.subscribe(({type}) => {
      if (["setExtractions", "updateValuations", "insertReports", "insertMarkings", "insertExtractions"].indexOf(type) != -1) {
        store.commit("setAllValuated", false)
      }
    })
  }
];
