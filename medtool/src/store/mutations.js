
import {getReportId, clearQueue} from "@/helpers.js"
import _ from "lodash"

export default {

  /* === REPORTS === */

  /**
   * Insert new reports into the store
   * @param  {Object} state      The vuex state
   * @param  {Array} reports     Array of reports
   * @param  {Boolean} overwrite TRUE if new reports should overwrite existing ones
   */
  insertReports(state, {reports, overwrite}) {
    reports = reports.map(r => ({
      id: getReportId(r),
      patient: parseInt(r[0]) || r[0],
      docu: r[1],
      source: r.slice(2),
      marks: [],
      valuation: []
    }))
    // either filter the existing reports or the new reports
    if (overwrite) {
      state.reports = state.reports.filter(sr => !reports.some(r => r.id == sr.id))
    } else {
      reports = reports.filter(r => !state.reports.some(sr => r.id == sr.id));
    }
    state.reports = [
      ...state.reports,
      ...reports
    ];
  },
  /**
   * Insert a single report into the store
   * @param  {Object} state  The vuex state
   * @param  {Object} report The report
   */
  saveReport(state, report) {
    state.reports.push({
      ...report,
      id: report.id || getReportId([report.patient, report.docu, ...report.source]),
      marks: [],
      valuation: [],
      custom: true
    })
  },
  /**
   * Deletes a report with the given id
   * @param  {Object} state The vuex state
   * @param  {[type]} id    The id of the report
   */
  deleteReport(state, id) {
    state.reports.splice(state.reports.findIndex(r => r.id == id), 1);
  },
  /**
   * Delete all reports and clears the queue
   * @param  {Object} state      The vuex state
   */
  deleteReports(state) {
    state.reports = [];
    state.history = [];
    clearQueue();
  },

  /* === EXTRACTIONS === */

  /**
   * Insert new extractions into the store
   * @param  {Object} state       The vuex state
   * @param  {[type]} extractions The new extractions
   */
  insertExtractions(state, extractions) {
    state.extractions = state.extractions.concat(extractions);
  },
  /**
   * Sets the extractions in the store
   * @param  {Object} state The vuex state
   * @param {[type]} e      The extraction erray
   */
  setExtractions(state, e) {
    state.extractions = e;
  },
  /**
   * Updates the extraction results of reports
   * @param  {Object} state The vuex state
   * @param  {[type]} extr  The extraction results
   */
  updateExtractions(state, extr) {
    extr.forEach(ex => {
      let report = state.reports.find(r => r.id == ex.id);
      if (report) report.extractions = ex.results;
    });
  },

  /* === MARKINGS === */

  /**
   * Sets a new marking of a report
   * @param  {Object} state  The vuex state
   * @param  {String} id     The report id
   * @param  {Number} column The column of the marking
   * @param  {Number} start  The start index of the marking
   * @param  {Number} end    The end index of the marking
   * @param  {Number} value  The value of the marking
   */
  updateMarks(state, {id, column, start, end, value}) {
    let report = state.reports.find(r => r.id == id);
    if (!report) return;
    if (value != "mn") {
      // remove not-neutral marks at the same location
      report.marks = report.marks.filter(m => m.column != column || m.i < start || m.i > end || m.value == "mn");
    } else {
      // remove neutral marks at the same location
      report.marks = report.marks.filter(m => m.column != column || m.i < start || m.i > end || m.value != "mn");
    }
    for (let i = start; i <= end; i++) {
      report.marks.push({column, i, value})
    }
  },
  /**
   * Erases a marking of a report
   * @param  {Object} state  The vuex state
   * @param  {String} id     The report id
   * @param  {Number} column The column of the marking
   * @param  {Number} start  The start index of the marking
   * @param  {Number} end    The end index of the marking
   */
  eraseMarks(state, {id, column, start, end}) {
    let report = state.reports.find(r => r.id == id);
    if (!report) return;
    report.marks = report.marks.filter(m => m.column != column || m.i < start || m.i > end);
  },
  /**
   * Resets all marks of a report
   * @param  {Object} state  The vuex state
   * @param  {String} id     The report id
   */
  resetMarks(state, id) {
    let report = state.reports.find(r => r.id == id);
    if (!report) return;
    report.marks = [];
  },
  /**
   * Insert imported markings
   * @param  {Object}  state     The vuex state
   * @param  {Array}   markings  The imported markings
   * @param  {Boolean} overwrite TRUE if duplicate markings should be overwritten
   */
  insertMarkings(state, {markings, overwrite}) {
    // either filter the existing markings or the new markings for duplicates
    if (overwrite) {
      state.importedMarkings = state.importedMarkings.filter(iv => !markings.some(v => v.text == iv.text))
    } else {
      markings = markings.filter(v => !state.importedMarkings.some(iv => v.text == iv.text));
    }
    state.importedMarkings = [
      ...state.importedMarkings,
      ...markings
    ];
  },
  /**
   * Delete all imported markings
   * @param  {Object} state The vuex state
   */
  deleteMarkings(state) {
    state.importedMarkings = [];
  },

  /* === VALUATION === */

  /**
   * Sets the valuation results
   * @param  {Object} state      The vuex state
   * @param  {Array}  valuations The computed valuations
   * @param  {Array}  markings   The computed markings
   */
  updateValuations(state, {valuations, markings}) {
    state.valuations = valuations;
    state.markings = markings;
  },
  /**
   * Sets the valuation results of reports
   * @param {Object} state The vuex state
   * @param {Array}  items The valuation results
   */
  setReportValuations(state, items) {
    for (let report of state.reports) {
      let item = items.find(i => i.id == report.id);
      if (!item) continue;
      report.valuation = item.valuation;
      report.result = item.result;
    }
  },
  /**
   * Clears the queue
   * @param  {Object} state The vuex state
   */
  clearQueue() {
    clearQueue();
  },
  /**
   * Sets the allValuated flag
   * @param {Object}  state The vuex state
   * @param {Boolean} v     The flag
   */
  setAllValuated(state, v) {
    state.allValuated = v;
  },

  /* === HISTORY === */

  /**
   * Clears the history
   * @param {Object} state The vuex state
   */
  clearHistory(state) {
    state.history = []
  },
  /**
   * Adds an item to the history
   * @param  {Object} state The vuex state
   * @param  {Object} h     The history item
   */
  pushHistory(state, h) {
    state.history.push({...h, time: Date.now()});
  },
  /**
   * Undo a single change
   * @param  {Object} state  The vuex state
   * @param  {String} type   The type of the change
   * @param  {Number} time   The time of the change
   * @param  {String} id     The id of the report
   * @param  {Array}  before The marking state of the report before the change
   */
  undo(state, {type, time, id, before}) {
    let report = state.reports.find(r => r.id == id);
    if (!report) return;
    if (type == "updateMarks" || type == "eraseMarks" || type == "resetMarks") {
      report.marks = before;
    }
    state.history.splice(state.history.findIndex(e => e.time == time), 1);
  },
  /**
   * Undo all changes of a report
   * @param  {Object} state   The vuex state
   * @param  {Array}  entries The history items
   */
  undoAll(state, entries) {
    let first = _.minBy(entries, 'time');
    let report = state.reports.find(r => r.id == first.id);
    if (!report) return;
    if (first.type == "updateMarks" || first.type == "eraseMarks" || first.type == "resetMarks") {
      report.marks = first.before;
    }
    state.history = state.history.filter(h => !entries.find(e => e.time == h.time));
  },

  /* === MISC === */

  /**
   * Toggles the traffic light
   * @param {Object}  state The vuex state
   * @param {Boolean} t     The light state
   */
  toggleTrafficLight(state, t) {
    state.showTrafficLight = t;
  },
  /**
   * Toggles live valuation
   * @param {Object} state The vuex state
   * @param {Boolean} l    The live state
   */
  setLive(state, l) {
    state.live = l
  },
  /**
   * Set the sorting type
   * @param {Object} state The vuex state
   * @param {String} s     The sorting type
   */
  setSortBy(state, s) {
    state.sortBy = s
  },
  /**
   * Set the sorting order
   * @param {Object} state The vuex state
   * @param {Boolean} s    TRUE if descending
   */
  setSortOrder(state, s) {
    state.sortOrder = s
  },
  /**
   * Set the visible report columns
   * @param {Object} state The vuex state
   * @param {Array}  c     The selected report columns
   */
  selectReportColumns(state, c) {
    let columns = [...c];
    columns.sort();
    state.selectedReportColumns = columns;
  }

}
