export default {
  reports: [],
  valuations: [],
  markings: [],
  importedMarkings: [],
  extractions: [],
  history: [],

  allValuated: false,

  reportColumns: ["Findings", "Anamnesis", "Assessment", "Technique"],
  selectedReportColumns: [0, 2],

  live: false,
  sortBy: 0,
  sortOrder: 0,
  showTrafficLight: false
};
