import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import state from "./state";
import mutations from "./mutations";
import actions from "./actions";
import plugins from "./plugins";

export default new Vuex.Store({
  state, mutations,
  getters: {
    /**
     * Get a report by id
     * @param  {Object} state The vuex state
     * @return {Function}     The getter function
     */
    getReport(state) {
      return (id) => state.reports.find(r => r.id == id)
    }
  },
  actions, plugins
})
