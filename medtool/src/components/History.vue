<template>
<v-card class="history">
  <!-- One history group per report -->
  <div class="history-group" v-for="(group, i) in history" :key="i">
    <div class="history-header" v-ripple @click="goTo(group.id)">
      <!-- The history header containing the report id an time of last change -->
      <div>
        <span class="subheading">Report {{group.id}}</span><br />
        <span class="body-2">{{new Date(group.time).toLocaleString()}}</span>
      </div>
      <!-- History actions -->
      <div class="history-actions">
        <!-- The Undo One action reverting the last change to a report -->
        <v-tooltip v-if="group.entries.length > 1" bottom :open-delay="500" content-class="tooltip">
          <template v-slot:activator="{ on }">
            <v-btn v-on="on" small icon @click.stop="undo(group)">
              <v-icon>undo</v-icon>
            </v-btn>
          </template>
          <span>Undo One</span>
        </v-tooltip>
        <!-- The Undo All action reverting all changes to a report -->
        <v-tooltip bottom :open-delay="500" content-class="tooltip">
          <template v-slot:activator="{ on }">
            <v-btn v-on="on" small icon @click.stop="undoAll(group)">
              <v-icon>replay</v-icon>
            </v-btn>
          </template>
          <span>Undo All</span>
        </v-tooltip>
      </div>
    </div>
    <!-- Report changes -->
    <div v-for="entry in group.entries" :key="entry.time" class="history-item">
      <!-- The change type -->
      <span class="history-label">{{entry.type == 'updateMarks' ? 'Change' : entry.type == 'eraseMarks' ? 'Erase' : 'Reset'}}</span>
      <!-- Preview of the changes markings -->
      <div class="history-preview">
        <!-- The markings before the change -->
        <span v-for="(word, i) in entry.wordsBefore" :class="word.class" :key="i+'b'">
          {{word.text}}
        </span>
        <span class="mx-1">
          <v-icon small color="black">mdi-arrow-right-circle</v-icon>
        </span>
        <!-- The markings after the change -->
        <span v-for="(word, i) in entry.wordsAfter" :class="word.class" :key="i+'a'">
          {{word.text}}
        </span>
      </div>
    </div>
  </div>
</v-card>
</template>

<script>
import _ from "lodash"

/**
 * The marking history dialog showing all changes
 * grouped by report and ordered by time
 */
export default {
  computed: {
    /**
     * The history groups with the changes for each report
     * @return {Array} The history groups
     */
    history() {
      return this.$store.state.history.map(e => {
        e = {
          ...e
        }; // shallow copy of e
        if (e.type == "updateMarks" || e.type == "eraseMarks") {
          // create change items with marking classes
          let words = e.source[e.payload.column].split(" ").slice(e.payload.start, e.payload.end + 1);
          let mapMarks = (marks) => words.map((w, i) => ({
            text: w,
            class: marks
              .filter(m => m.column == e.payload.column && m.i == e.payload.start + i)
              .map(m => m.value).join(" ")
          }))
          // set state before and after the change
          e.wordsBefore = mapMarks(e.before);
          e.wordsAfter = mapMarks(e.after);
        } else if (e.type == "resetMarks") {
          // create summary of changes
          let words = _.flatten(e.source.map((c, column) => {
            let ws = c.split(" ")
              .map((w, i) => ({
                text: w,
                i,
                column
              }))
              .filter(({
                i
              }) => e.before.find(b => b.column == column && b.i == i))
            if (ws.length > 0)
              return ws.reduce((ws, word) => {
                if (ws[ws.length - 1].text == '...' || ws[ws.length - 1].i == word.i - 1) {
                  ws.push(word)
                } else {
                  ws.push({
                    text: '...',
                    i: -1,
                    column: -1
                  })
                  ws.push(word);
                }
                return ws;
              }, [{
                text: '...',
                i: -1,
                column: -1
              }])
            else return [];
          }))
          words.splice(0, 1);
          const maxlen = 10;
          if (words.length > maxlen) {
            if (words[maxlen].text == "...") {
              words = words.slice(0, maxlen + 1);
            } else {
              words = [...words.slice(0, maxlen + 1), {
                text: "...",
                i: -1,
                column: -1
              }]
            }
          }
          // set state before the changes
          e.wordsBefore = words.map(({
            text,
            column,
            i
          }) => ({
            text,
            class: e.before
              .filter(m => m.column == column && m.i == i)
              .map(m => m.value).join(" ")
          }))
          // set state after the changes - no markings, resetted
          e.wordsAfter = words.map(({
            text
          }) => ({
            text,
            class: ""
          }))
        }
        return e;
      }).reduce((hist, e) => {
        // group history by report id
        if (hist.length == 0) return [
          [e]
        ]
        if (hist[hist.length - 1][0].id == e.id) {
          hist[hist.length - 1].push(e)
        } else {
          hist.push([e])
        }
        return hist;
      }, []).map(g => ({
        entries: g.reverse(),
        id: g[0].id,
        time: g[0].time
      })).reverse(); // display most recent changes first
    }
  },
  methods: {
    /**
     * Undo the last change of a history group
     * @param  {Array} entries The entries in the selected history group
     */
    undo({
      entries
    }) {
      this.$store.commit("undo", entries[0])
    },
    /**
     * Undo all changes of a history group
     * @param  {Array} entries The entries in the selected history group
     */
    undoAll({
      entries
    }) {
      this.$store.commit("undoAll", entries)
    },
    /**
     * Open a report
     * @param  {String} id The id of the selected report
     */
    goTo(id) {
      this.$router.push({
        name: "marking",
        params: {
          reportId: id
        }
      });
    },
  }
}
</script>

<style scoped lang="scss">
.history {

    display: flex;
    flex-flow: column nowrap;
    width: 400px;
    max-height: 80vh;
    overflow-y: scroll;

    .history-group {
        background: #eee;
        padding-bottom: 1em;
    }

    .history-header {
        padding: 1em;
        background: white;
        margin-bottom: 1em;
        box-shadow: 0 0 3px rgba(0,0,0,.2);
        display: flex;
        justify-content: space-between;
        align-items: center;
        line-height: 1.3em;
        transition: background-color 0.3s;
        cursor: pointer;

        .history-actions button {
            margin: 0 8px;
        }

        &:hover {
            background: #efefef;
        }
    }

    .history-item {

        padding: 0 1em;

        .history-label {
            text-transform: uppercase;
            font-size: 0.8em;
            font-weight: 600;
        }

        .history-preview {
            font-size: 0.9em;
        }
    }
}
</style>
