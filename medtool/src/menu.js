const {Menu} = require('electron')
const electron = require('electron')
const app = electron.app

// set the application menu and submenu items
const template = [
  {
    label: 'File',
    submenu: [
      {
        label: "Open Reports",
        accelerator: "CmdOrCtrl+O",
        click(_, win) { win.webContents.send("file", "open"); }
      },
      {
        label: "Save Results",
        accelerator: "CmdOrCtrl+S",
        click(_, win) { win.webContents.send("file", "save"); }
      },
      {
        type: "separator"
      },
      {
        label: "Import",
        submenu: [{
          label: "Markings",
          accelerator: "CmdOrCtrl+I",
          click(_, win) { win.webContents.send("file", "importMarkings"); }
        }, {
          label: "Extractions",
          click(_, win) { win.webContents.send("file", "importExtractions"); }
        }]
      }, {
        label: "Export",
        submenu: [{
          label: "Markings",
          accelerator: "CmdOrCtrl+E",
          click(_, win) { win.webContents.send("file", "exportValuations"); }
        }, {
          label: "Extractions",
          click(_, win) { win.webContents.send("file", "exportExtractions"); }
        }]
      }
    ]
  },
  {
    label: "Edit",
    submenu: [
      { role: 'undo' },
      { role: 'redo' },
      { type: 'separator' },
      { role: 'cut' },
      { role: 'copy' },
      { role: 'paste' },
      { role: 'delete' },
      { type: "separator" },
      {
        label: "Clear History",
        click(_, win) { win.webContents.send("file", "clearHistory"); }
      },
      {
        label: "Clear Reports",
        click(_, win) { win.webContents.send("file", "deleteReports"); }
      },
      {
        label: "Clear Imported Markings",
        click(_, win) { win.webContents.send("file", "deleteMarkings"); }
      },
      { type: 'separator' },
      { role: 'selectAll' }
    ]
  },
  {
    label: "Modules",
    submenu: [{
      label: "Marking Module",
      accelerator: "CmdOrCtrl+Alt+1",
      click(_, win) { win.webContents.send("module", "marking"); }
    },{
      label: "Writing Module",
      accelerator: "CmdOrCtrl+Alt+2",
      click(_, win) { win.webContents.send("module", "writing"); }
    },{
      label: "Values Module",
      accelerator: "CmdOrCtrl+Alt+3",
      click(_, win) { win.webContents.send("module", "extraction"); }
    },{
      label: "Reports Module",
      accelerator: "CmdOrCtrl+Alt+4",
      click(_, win) { win.webContents.send("module", "reports"); }
    }]
  },
  {
    role: 'window',
    submenu: [
      {
        role: 'minimize'
      },
      {
        role: 'close'
      }
    ]
  }
]

// add special items for macOS
if (process.platform === 'darwin') {
  const name = app.getName()
  template.unshift({
    label: name,
    submenu: [
      {
        role: 'about'
      },
      {
        type: 'separator'
      },
      {
        role: 'services',
        submenu: []
      },
      {
        type: 'separator'
      },
      {
        role: 'hide'
      },
      {
        role: 'hideothers'
      },
      {
        role: 'unhide'
      },
      {
        type: 'separator'
      },
      {
        role: 'quit'
      }
    ]
  })

  // Window menu
  template[4].submenu = [
    {
      label: 'Close',
      accelerator: 'CmdOrCtrl+W',
      role: 'close'
    },
    {
      label: 'Minimize',
      accelerator: 'CmdOrCtrl+M',
      role: 'minimize'
    },
    {
      label: 'Zoom',
      role: 'zoom'
    },
    {
      type: 'separator'
    },
    {
      label: 'Bring All to Front',
      role: 'front'
    }
  ]
}

// add special items for development
if (process.env.NODE_ENV !== 'production') {
  template.push({
    label: 'View',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click (_, focusedWindow) {
          if (focusedWindow) focusedWindow.reload()
        }
      },
      {
        label: 'Toggle Developer Tools',
        accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
        click (_, focusedWindow) {
          if (focusedWindow) focusedWindow.webContents.toggleDevTools()
        }
      },
      {
        type: 'separator'
      },
      {
        role: 'resetzoom'
      },
      {
        role: 'zoomin'
      },
      {
        role: 'zoomout'
      },
      {
        type: 'separator'
      },
      {
        role: 'togglefullscreen'
      }
    ]
  })
}

// build and set the menu
const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)
