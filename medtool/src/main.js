import '@babel/polyfill'

import Vue from 'vue'

// import all needed fonts and styles
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.min.css'
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

import App from './App.vue'
import router from './router'
import store from './store/'

// set Page and Layout as global components
import Page from "@/components/Page"
import Layout from "@/components/Layout"
Vue.component("Page", Page)
Vue.component("Layout", Layout)

// import all used vue plugins

import Vuetify from 'vuetify'
Vue.use(Vuetify);
const vuetify = new Vuetify({
  icons: {
    iconfont: 'md'
  }
})

import VueMasonry from 'vue-masonry-css';
Vue.use(VueMasonry);

import chroma from "chroma-js"
Vue.mixin({data: () => ({chroma})})

import Notifications from 'vue-notification'
Vue.use(Notifications)

import VueApexCharts from 'vue-apexcharts'
Vue.component('chart', VueApexCharts)

import "prismjs";
import "prismjs/themes/prism.css";
import "prismjs/components/prism-regex";

// create a new vue instance
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
