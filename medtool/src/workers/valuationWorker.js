importScripts('./lodash.js');
importScripts('./utils.js');

/**
 * This worker performs the second and final phase of the valuation algorithm and computes the valuation
 * results for the provided reports. This is done by matching the valuations to the contents of the report
 * and selecting the best fitting ones by preferring the largest possible context.
 * @param  {Object} data The data object defining valuations and reports
 * @return {Object}      EITHER A progress message OR The valuation results for each report
 */
self.onmessage = ({data}) => {
  console.log("Start valuating "+data.reports.length+" reports.");
  postMessage({
    type: "finish",
    data: data.reports.map((report, i) => { // iterate over each report
      let valuation = reportValuation(report, data.valuations); // compute the matched valuations
      let result = utils.valuate(report.source, valuation, report.extractions); // compute the valuation result from the matches
      postMessage({type: "progress", data: i}); // post the progress
      return {result, valuation};
    })
  });
  console.log("VALUATE FINISHED")
}

/**
 * Computes the matched valuations for a report
 * @param  {Object} report The report
 * @param  {Array}  vals   The valuations
 * @return {Array}         The matched valuations
 */
function reportValuation(report, vals) {
  if (!report) return [];
  let valuations = [];

  // helper function to determine the type of valuation
  let value = (v) => v.normal>0?'normal':v.preabnormal>0?'preabnormal':'abnormal';

  // iterate over the report columns
  for (let c in report.source) {

    // transform the report column to a list of words
    let words = utils.splitText(report.source[c]).map(a => utils.degrammar(a.delim+a.text) || "");
    let str = words.filter(a => a != "").join(" ");

    // filter the valuations that are occurring in the current column
    // sort the valuations - most words first
    let columnVals = _.sortBy(vals.filter(v => str.includes(v.word)), v => 1/v.word.split(" ").length);

    // iterate over the valuations, most words first
    for (let v of columnVals) {
      let [i, j] = subArrayInterval(words, v.word); // find the indexes of the occurrence
      if (i >= 0) {
        // prepare the valuation
        let ev = {
          column: c,
          start: i,
          end: j,
          value: value(v),
          stat: v,
          weight: v[value(v)]*v.word.split(" ").length
        }

        // only add to the array if text is not yet covered by another valuation
        if (!valuations.some(e => e.column == ev.column && e.start <= ev.start && e.end >= ev.end)) {
        // if (!valuations.some(e => e.column == ev.column && ((e.start <= ev.start && e.end >= ev.start) || (e.start <= ev.end && e.end >= ev.end)))) {
          valuations.push(ev);
        }
      }
    }
  }
  return valuations;
}

/**
 * Finds the start and end index of a string inside an array of words.
 * Returns [-1, -1] if the string is not found.
 * @param  {Array}  a   The array of words
 * @param  {String} str The string
 * @return {Array}      The start and end index.
 */
function subArrayInterval(a, str) {
  let b = str.split(" "); // split the string into words
  let j = 0, start = 0;
  for (let i = 0; i < a.length; i++) { // iterate over each entry of the array
    if (a[i] == "") continue; // ignore empty words
    let c = a[i].split(" "); // some entries in a may contain more than one word
    for (let k = 0; k < c.length; k++) { // iterate over each word of an entry
      if (c[k] == b[j]) { // found a matching word, proceed to the next word of the string
        if (j == 0) start = i;
        j++;
        if (j == b.length) { // reached the end of the word
          return [start, i];
        }
      } else { // a word doesn't match, start over at the first word of the string
        j = 0;
      }
    }
  }
  return [-1, -1];
}
