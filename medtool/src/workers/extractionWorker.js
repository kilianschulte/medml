importScripts('./lodash.js');

/**
 * This worker handles the computation of numeric extractions. Although the computation is not
 * that performance heavy to require a seperate thread, the worker is used to execute the custom
 * code written by the user in a secure context decoupled from the main application.
 * @param  {Object} data The data object defining extractions and reports
 * @return {Array}      The extractions for each report
 */
self.onmessage = ({data}) => {

  let extractions = data.extractions, reports = data.reports;

  postMessage(reports
    .map(r => ({
      id: r.id,
      results: extractions
        .map(ex => {
          // get the wrapper functions
          let map = getMapFunc(ex.map.code);
          let agg = getAggFunc(ex.agg.code, ex.agg.init);
          let val = getValFunc(ex.val.code);

          let source = r.source.filter((_, i) => ex.columns.indexOf(i) != -1).join(" ");
          let result = extract(ex.searchExp, source.toLowerCase(), map, agg, val);
          return result ? { name: ex.name, source, weight: ex.weight, ...result } : null
        })
        .filter(r => r != null)
    }))
    .filter(r => r.results.length > 0)
  );
};

/**
 * Performs an extraction on the given report
 * @param  {String}   search  The search expression
 * @param  {Array}    source  The report source
 * @param  {Function} mapFunc The mapping function
 * @param  {Function} aggFunc The aggregation function
 * @param  {Function} valFunc The valuation function
 * @return {Object}           The extraction result
 */
function extract(search, source, mapFunc, aggFunc, valFunc) {
  let regex = new RegExp(search, "gi");
  let match, res = [];

  // iterate over each match
  while ((match = regex.exec(source)) !== null) {
    res.push({
      match: match.slice(1).map(e => e == undefined ? "null" : e).join(";"), // used for the debugging table
      mapped: mapFunc(match.slice(1).map(e => isNaN(Number(e)) ? e : Number(e))) // call map function on parsed match
    });
  }
  if (res.length == 0) return null;
  // aggregate matches
  let aggValue = aggFunc(res.map(r => r.mapped));
  // compute valuation
  let valValue = valFunc(aggValue);
  return {map: res, agg: aggValue, val: valValue};
}

/**
 * Creates a map function from the given code
 * @param  {String}   code The function code
 * @return {Function}      The map function
 */
function getMapFunc(code) {
  return (a) => {
    let exp = a.reduce((exp, e, i) => exp // process the template placeholders
      .split(`{${i}}`)
      .join(typeof e === "number" ? e : typeof e === "string" ? `"${e}"` : ""+e)
    , code);
    return evalMap(exp); // evaluate the expression
  }
}

/**
 * Creates a aggregate function from the given code
 * @param  {String}   code The function code
 * @param  {any}      init The initial value
 * @return {Function}      The aggregate function
 */
function getAggFunc(code, init) {
  return (array) => array.reduce((agg, e, i, elems) => { // aggregate using reduce
    return evalAgg(code, _.cloneDeep(agg), _.cloneDeep(e), _.cloneDeep(i), _.cloneDeep(elems)); // evaluate the code, clone to prevent side-effects
  }, evalInit(init));
}

/**
 * Create a valuate function from the given code
 * @param  {String}   code The function code
 * @return {Function}      The valuate function
 */
function getValFunc(code) {
  return (val) => Math.min(1, Math.max(0, evalVal(code, val) || 0)); // limit between 0 and 1
}

/**
 * Debounced error function when the evaluation of the code throws an error
 * @type {Function}
 */
const onError = _.debounce((e) => postMessage({err: e}), 500);

/**
 * Evaluates a map expression
 * @param  {String} exp The map expression
 * @return {any}        The mapped value
 */
function evalMap(exp) {
  try {
    return eval(exp); // use the js eval function
  } catch (err) {
    onError({name: err.name, message: err.message, method: "Map Expression"}); // send the thrown error
  }
}

/**
 * Evaluates a init expression
 * @param  {String} exp The init expression
 * @return {any}        The init value
 */
function evalInit(exp) {
  try {
    return eval(exp); // use the js eval function
  } catch (err) {
    onError({name: err.name, message: err.message, method: "Aggregate Init Expression"}); // send the thrown error
  }
}

/**
 * Evaluates a aggregation expression
 * @param  {String} exp   The aggregation expression
 * @param  {any}    agg   The current aggregated value
 * @param  {any}    e     The current element
 * @param  {Number} i     The current index
 * @param  {Array}  elems The elements array
 * @return {any}          The next aggregated value
 */
function evalAgg(exp, agg, e, i, elems) {
  try {
    return eval(exp); // use the js eval function
  } catch (err) {
    onError({name: err.name, message: err.message, method: "Aggregate Expression"}); // send the thrown error
  }
}

/**
 * Evaluates a valuation expression
 * @param  {String} exp   The valuation expression
 * @param  {any}    value The aggregated value
 * @return {any}          The valuation value
 */
function evalVal(exp, value) {
  try {
    return eval(exp); // use the js eval function
  } catch (err) {
    onError({name: err.name, message: err.message, method: "Valuate Expression"}); // send the thrown error
  }
}
