importScripts('./lodash.js');
importScripts('./utils.js');

/**
 * This worker performs the first phase of the valuation algorithm and computes a set of
 * valuations from the provided raw markings. This includes looking for unambigous phrases that
 * can be assigned to a single valuation type as well as counting the occurrence of those phrases
 * which are marked several times.
 * The worker is called automatically each time the markings are updated.
 * @param  {Object} data The data object defining markings and neutrals
 * @return {Array}       The computed valuations
 */
self.onmessage = ({data}) => {
  console.time("computeValuations"); // measure the time
  let result = computeValuations(data.markings, data.neutrals);
  console.timeEnd("computeValuations");
  postMessage(result);
}

/**
 * Computes the valuations from markings
 * @param  {Array} marks     The markings
 * @param  {Array} neutrals  The neutral markings
 * @return {Object}          The computed valuations
 */
function computeValuations(markings, neutrals) {

  let allMarkings = [], valNeutrals = [], words = {};

  for (let n of neutrals) {
    let neut = {
      text: utils.degrammar(n.text),
      source: n.text,
      id: n.id,
      value: 'neutral'
    }

    allMarkings.push(neut);
    valNeutrals = valNeutrals.concat(
      neut.text.split(" ") // compute the potency volume of words
      .reduce((pot, s, i) => [
        ...pot, {s, i},
        ...pot.filter(p => p.i == i-1).map(p => ({s: p.s + " " + s, i}))
      ], [])
      .map(p => p.s)
    )

  }

  for (let m of markings) {

    let mark = {
      text: utils.degrammar(m.text),
      source: m.text,
      id: m.id,
      value: m.value
    }

    allMarkings.push(mark);

    let mwords = mark.text.split(" ");

    for (let n = 1; n <= mwords.length; n++) {
      for (let i = 0; i <= mwords.length-n; i++) {
        let chunk = mwords.slice(i, i+n).join(" ");

        if (valNeutrals.includes(chunk)) continue;

        if (!words[chunk]) {
          words[chunk] = {
            normal: 0, preabnormal: 0, abnormal: 0,
            sources: [], word: chunk
          }
        }
        words[chunk][mark.value]++;
        words[chunk].sources.push(({source: mark.source, value: mark.value}));
      }
    }

  }

  let valuations = [];

  for (let w in words) {
    let i = (words[w].normal == 0) + (words[w].preabnormal == 0) + (words[w].abnormal == 0);
    if (i == 2) { // only add to array if valuation is deterministic (has occurrences only under one type of marking)
      if (words[w].word.split(" ").some(word => word.length > 3 && isNaN(word))) { // only add if some word length is > 3 and and not all words are numbers
        valuations.push(words[w]);
      }
    }
  }

  return { valuations, markings: allMarkings };
}
