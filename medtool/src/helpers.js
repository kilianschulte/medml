
import stringHash from "string-hash";
import _ from "lodash"
import fs from "fs";
import csv from "csvtojson";

const { dialog } = require('electron').remote;

import ValuationWorker from 'worker-loader!@/workers/valuationWorker.js';

import utils from "@/../public/utils";

export let degrammar = utils.degrammar;
export let valuate = utils.valuate;
export let splitText = utils.splitText;

/**
 * Create a hash from the report content to use as a report id
 * TODO: check uniqueness
 * @param {Array} report The report source
 */
export let getReportId = (report) => {
  return stringHash(report.join(""));
}

/**
 * Get all markings of a specific type for a specific report
 * @param  {Object}  report    The report
 * @param  {String}  mark      The marking type
 * @param  {Boolean} keepWords Keep as single words or combine to phrases
 * @return {Array}             The markings
 */
export let getMarkings = (report, mark, keepWords) => {
  let words = report.source.map(splitText);
  if (keepWords) {
    let marks = report.marks.filter(m => m.value == mark);
    return marks.map(m => words[m.column][m.i].text).filter(t => t != "");
  } else {
    let markings = [];
    for (let column = 0; column <= report.source.length; column++) {
      let marks = report.marks.filter(m => m.value == mark && m.column == column);
      if (marks.length == 0) continue;
      marks.sort((a, b) => a.i - b.i);
      let rmarking = {words: []};
      for (let mark of marks) {
        if (rmarking.words.length == 0) {
          rmarking.words.push(words[mark.column][mark.i]);
          rmarking.end = mark.i;
        } else {
          if (rmarking.end != mark.i-1) {
            markings.push(rmarking.words.map(w => w.delim+w.text).join(""));
            rmarking.words = [];
          }
          rmarking.words.push(words[mark.column][mark.i]);
          rmarking.end = mark.i;
        }
      }
      markings.push(rmarking.words.map(w => w.delim+w.text).join(""));
    }
    return markings;
  }
}

/**
 * Open and parse a csv file
 * @param  {String} title The open dialog title
 * @return {Promise}      Promise that resolves to the parsed file content
 */
export let openCSV = (title) => {
  return new Promise((resolve, reject) => {
    dialog.showOpenDialog(
      {title, filters: [{name: "Table", extensions: ["csv"]}], properties: ["openFile"]},
      (files) => {
        if (files && files[0])
          fs.readFile(files[0], 'utf8', (err, data) => {
            if (err) reject(err);
            else csv({noheader: true, output: "csv"})
              .fromString(data)
              .then(resolve)
              .catch(reject)
          })
        else resolve();
      }
    )
  })
}

/* ==== VALUATION HANDLING ==== */

/**
 * Number of reports to send to one valuation thread
 * @type {Number}
 */
const BULK_SIZE = 150;
/**
 * Maximum number of threads / workers to spawn
 * @type {Number}
 */
const WORKER_COUNT = 5;
/**
 * Creates an array of worker threads for valuation
 * @type {Array}
 */
const workers = new Array(WORKER_COUNT).fill(0).map(() => ({running: false, worker: new ValuationWorker()}));
/**
 * The queue of scheduled reports for valuation
 * @type {Array}
 */
let queue = [];
/**
 * Structure to track and subscribe to the progress of the worker threads
 * @type {Object}
 */
export let progress = {
  __value: 0,
  __queued: 0,
  __callbacks: [],
  increase() {
    this.__value++;
    this.notify();
  },
  reset() {
    this.__value = 0;
    this.__queued = 0;
    this.notify();
  },
  setQueue(q) {
    this.__queued = q;
  },
  subscribe(cb) {
    this.__callbacks.push(cb);
  },
  notify() {
    this.__callbacks.forEach(cb => cb(this.__value, this.__queued));
  }
}

/**
 * Select at max BULK_SIZE scheduled reports with the highest priorities and send to an idle worker
 * @param  {Object}   store The vuex store
 * @param  {Object} cb      The callback structure to notify when all workers are finished and the queue is empty
 */
export let runWorkers = (store, cb) => {
  progress.setQueue(queue.length);
  if (queue.length == 0 && workers.every(w => !w.running) && cb && cb.onFinish) {
    cb.onFinish(); // call the callback
    delete cb.onFinish; // delete the callback so that it only gets called once
  }

  if (workers.every(w => w.running) || queue.length == 0) return; // all workers are running
  let w = workers.find(w => !w.running); // select an idle worker
  w.running = true;

  let next = _.maxBy(queue, 'prio'); // find the report with highest priority

  // partition the reports by priority and extract at most BULK_SIZE reports with the same priority as max. priority
  let partitions = _.partition(queue, {prio: next.prio});
  queue = [...partitions[0].slice(BULK_SIZE), ...partitions[1]];
  let scheduled = partitions[0].slice(0, BULK_SIZE);

  progress.setQueue(queue.length); // update the queue size

  if (workers.some(w => !w.running) && queue.length > 0) {
    // directly start more workers until there are no more idle workers or the queue is empty
    setImmediate(() => runWorkers(store, cb));
  }

  // send the selected reports to the worker
  w.worker.postMessage({
    reports: scheduled.map(s => s.report),
    valuations: store.state.valuations
  })
  // set the worker callback
  w.worker.onmessage = ({data}) => {
    if (data.type == "progress") {
      progress.increase(); // increase the progress
      return;
    }
    data = data.data;

    let toCommit = [];
    for (let i = 0; i < scheduled.length; i++) {
      // seperate the reports by ones with callback and ones without
      // commit only the reports without callback
      if (scheduled[i].cb) {
        scheduled[i].cb(scheduled[i].report.id, data[i].valuation, data[i].result);
      } else {
        toCommit.push({
          id: scheduled[i].report.id,
          valuation: data[i].valuation,
          result: data[i].result
        })
      }
    }

    store.commit("setReportValuations", toCommit);
    w.running = false;

    // console.log("FINISHED "+i+" "+next.prio+" "+next.report.id)
    // start another worker
    setImmediate(() => runWorkers(store, cb));
  }
}

/**
 * Clears the queue
 */
export let clearQueue = () => {
  queue = [];
  progress.setQueue(0);
};
/**
 * Replaces the queue
 * @param {Array} q The new queue
 */
export let setQueue = (q) => {
  queue = q;
  progress.setQueue(q.length);
};
/**
 * Returns the queue
 * @return {Array} The queue
 */
export let getQueue = () => queue;
