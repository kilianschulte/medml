(function() {
  /** Detect free variable `global` from Node.js. */
  var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

  /** Detect free variable `self`. */
  var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

  /** Used as a reference to the global object. */
  var root = freeGlobal || freeSelf || Function('return this')();

  /** Detect free variable `exports`. */
  var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

  /**
   * Configuration for degrammifying a text
   * @type {Object}
   * @prop {Array} unify Array of endings to unify cases of words
   * @prop {Array} clear Array of words to remove from the text
   * @example unifies "unauffälliges", "unauffälliger" to "unauffällig"
   */
  const config = {
    unify: [
      {
        stem: "ein",
        suffix: ["e", "es", "em", "en", "er"],
        allowStemOnly: true
      }, {
        stem: "isch",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "ung",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "aer",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "al",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "ior",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "llig",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "ant",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "lich",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "ert",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "ret",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "ent",
        suffix: ["e", "en", "es", "em", "er"]
      }, {
        stem: "nah",
        suffix: ["e", "es", "er", "em", "en"],
        allowStemOnly: true
      }, {
        stem: "dies",
        suffix: ["e", "es", "er", "em", "en"],
        allowStemOnly: true
      }
    ],
    clear: [
      "der", "das", "die", "in", "am", "an", "von",
      "des", "dem", "den", "bei", "im"
    ],
  };

  // transform unify items to regular expressions
  config.unify = config.unify.map(u =>
    new RegExp(`^(${u.allowStemOnly ? '\\S*' : '\\S+'}${u.stem})(?:${u.suffix.join("|")})?$`)
  )

  /**
   * Returns a degrammified string based on the above configuration
   * @param  {String} string String to degrammify
   * @return {String}        Resulting string
   */
  function degrammar(string) {
    return string.trim()
    .toLowerCase()                              // make lowercase
    .replace(/ä/g, "ae")                        // replace umlaut ä by ae
    .replace(/ö/g, "oe")                        // replace umlaut ö by oe
    .replace(/ü/g, "ue")                        // replace umlaut ü by ue
    .replace(/ß/g, "ss")                        // replace ß by ss
    .replace(/[."'`]/g, "")                     // remove . " ' `
    .replace(/[^\s\w\d]/g, (x)=>" "+x+" ")      // separate all characters that are not whitespaces, letters or digits
    .replace(/[;,-]|\(|\)/g, "")                // remove ; , - ( )
    .replace(/\s+/g, " ")                       // replace multiple whitespaces by a single one
    .trim()                                     // remove whitespaces at beginning and end
    .split(" ")                                 // split into words
    .map(w => config.unify.reduce((ww, r) =>    // unify endings
      (ww.match(r) || [0, ww])[1], w)
    )
    .filter(w => config.clear.indexOf(w) == -1) // remove words based on config
    .join(" ");                                 // combine back to string
  }

  /**
   * Split a string into words and preceeding delimiter
   * Delimiter is one of [ ] , ; ? ! .
   * @param  {String} str The string to split
   * @return {Array}      The array of words and delimiters
   */
  function splitText(str) {
    return str.split(/(?=[\s,;?!.])/)
    .map(a => a.match(/^([\s,;?!.]?)(.*)$/))
    .map(a => ({delim: a[1]||"", text: a[2]}))
  }

  /**
   * Calculates the valuation result of a report
   * @param  {Array<String>} source      The source strings of the report
   * @param  {Array<Object>} valuation   The matched valuations for the report
   * @param  {Array<Object>} extractions The extractions of the project
   * @return {Object}                    The calculated result
   */
  function valuate(source, valuation, extractions) {

    // calculate weighted valuations for the extractions
    let exWeight = extractions ? extractions.reduce((w, e) => w + e.val * e.weight, 0) : 0;
    // calculate sum of weights for the extractions
    let exSum = extractions ? extractions.reduce((sum, e) => sum + parseInt(e.weight), 0) : 0;

    // function calculating the weighted sum of valuations for a specific type
    const weightedSum = (k) => (a, i) => a + i.stat[k] * i.stat.word.split(" ").length;
    const f = (k) => valuation.reduce(weightedSum(k), 0);

    // probability of yellow marked sections being 'normal'
    // 1 makes yellow markings equal to green markings
    // 0 makes yellow markings equal to red markings
    const bFactor = 0.2;

    // gets the valuation values for each marking type
    let a = f("normal"),
      b = f("preabnormal"),
      c = f("abnormal");

    // the abnormality including extractions
    let abnormality = (a+b+c+exSum) > 0
      ? (c+b*(1-bFactor)+exWeight) / (exSum+a+b+c || 1)
      : 0.5;

    // the abnormality excluding extractions
    let abnormality_raw = (a+b+c) > 0
      ? (c+b*(1-bFactor)) / (a+b+c || 1)
      : 0.5;

    // console.log("Abnorm: "+((c+b*(1-bFactor)) / (a+b+c || 1))+", "+(exWeight / (exSum || 1))+" ("+(a+b+c)+", "+exSum+") => "+abnormality);

    // the coverage of valuations relative to the count of words of the report
    let coverage = Math.min((a+b+c)/source.map(s => s.split(" ").length).reduce((sum, a) => sum + a, 0), 1) || 0;

    // the ambiguity including extractions
    let ambiguity = 1-(Math.abs(a+b*bFactor-c-b*(1-bFactor))+exSum)/(a+c+b+exSum || 1);

    // the ambiguity excluding extractions
    let ambiguity_raw = 1-(Math.abs(a+b*bFactor-c-b*(1-bFactor)))/(a+c+b || 1);

    return {
      abnormality, coverage, ambiguity,
      abnormality_raw, ambiguity_raw
    }
  }

  const utils = {};

  utils.config = config;
  utils.degrammar = degrammar;
  utils.splitText = splitText;
  utils.valuate = valuate;

  if (freeModule) {
    // Export for Node.js.
    (freeModule.exports = utils).utils = utils;
    // Export for CommonJS support.
    freeExports.utils = utils;
  }
  else {
    // Export to the global object.
    root.utils = utils;
  }
}.call(this));
