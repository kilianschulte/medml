const fs = require("fs");
const _ = require("lodash");
let data = require("../befunde.json");

let blacklist = ["der"];

class Node {
  constructor(w, c) {
    this.word = w;
    this.childs = c || [];
    this.childs.forEach(c => c.parent = this);
    this.parent = null;
    this.count = 0;
  }
  increaseCount() {
    this.count++;
  }
  setWord(w) {
    this.word = w;
  }
  addChild(c) {
    this.childs.push(c);
    c.parent = this;
  }
  removeChild(w) {
    if (w instanceof Node)
      this.childs = this.childs.filter(c => c !== w);
    else
      this.childs = this.childs.filter(c => c.word == w)
  }
  hasChild(w) {
    return this.getChild(w) && true;
  }
  hasChildNode(n) {
    return this.childs.find(c => c == n) && true
  }
  hasWord(w) {
    return w === this.word;
  }
  getChild(w) {
    return this.childs.find(c => c.hasWord(w))
  }
  equals(node) {
    if (this.word instanceof Node) {
      return this.word.equals(node.word)
    } else if (node.word instanceof Node ){
      return false;
    } else {
      return this.word == node.word;
    }
  }
  print() {

    let w = this.word;
    if (w instanceof Node)
      w = w.print();

    if (this.childs.length > 0) {
      return {
        word: w,
        count: this.count,
        childs: _.reverse(_.sortBy(this.childs.map(c => c.print()), "count"))
      }
    } else {
      return {
        word: w,
        count: this.count
      };
    }

  }
}

class Word extends Node {
  constructor(w) {
    super(w, null);
  }
}

class WordOption extends Node {
  constructor(options, childs) {
    super(new Node(null, options), childs);
  }
  hasWord(w) {
    return this.word.hasChild(w)
  }
  equals(node) {
    for (let w of this.word.childs) {
      if (!node.hasWord(w)) {
        return false;
      }
    }
    return true;
  }
  print() {
    debugger;
    return {
      word: this.word.childs.map(c => c.print()),
      childs: this.childs.map(c => c.print())
    }
  }
}

let tree = new Word("");

for (let d of data) {
  if (!d[2]) continue;
  let sentences = d[2].split(".");
  for (let s of sentences) {
    let words = s.split(/[\s,-;]/g);
    let node = tree;
    for (let w of words) {
      if (w.length == 0) continue;
      if (!node.hasChild(w)) {
        node.addChild(new Word(w));
      }
      node = node.getChild(w);
      node.increaseCount();
    }
  }
}

let z = (n) => n < 10 ? "0"+n : n;
let t = (n) => n > 0 ? "-"+t(n-1) : "";

combinePaths = (tree, n) => {
  let i = 0;
  for (let child of tree.childs) {
    combinePaths(child, n+1);
    console.log(`${t(n)} | ${z(++i)} of ${z(tree.childs.length)} | Combined under ${child.word}`);
    if (child.childs.length > 1) {
      for (let w1 = 0; w1 < child.childs.length - 1; w1++) {
        for (let w2 = w1 + 1; w2 < child.childs.length; w2++) {
          findDoupling(child, child.childs[w1], child.childs[w2]);
        }
      }
    }
  }
}

findDoupling = (tree, w1, w2) => {
  if (w1.equals(w2)) {

    if (blacklist.indexOf(w1.word) !== -1)
      return;

    let grandchilds = [...w1.childs, ...w2.childs],
        childs = [new Node(w1.word, grandchilds)],
        rootchilds = [];

    let p1, p2;

    if (w1.parent !== tree) {
      childs = [...childs, ...w1.parent.childs.filter(c => c !== w1)];

      p1 = w1.parent;
      p1.childs = [];
      while (p1.parent && p1.parent !== tree) {
        p1 = p1.parent;
      }
    } else {
      p1 = new Word("");
    }

    if (w2.parent !== tree) {
      childs = [...childs, ...w2.parent.childs.filter(c => c !== w2)];

      p2 = w2.parent;
      p2.childs = [];
      while (p2.parent && p2.parent !== tree) {
        p2 = p2.parent;
      }
    } else {
      p2 = new Word("");
    }

    let filter = (c) => !(c == p1 || c == w1) && !(c == p2 || c == w2)

    if (w1.parent === tree || w2.parent === tree) {
      childs = [...childs, ...tree.childs.filter(filter)];
    } else {
      rootchilds = tree.childs.filter(filter);
    }

    tree.childs = rootchilds;
    tree.addChild(new WordOption([
      p1, p2
    ], childs))
  } else {
    let j1 = 0, c1 = w1;
    while (j1 <= w1.childs.length) {
      let j2 = 0, c2 = w2;
      while (j2 <= w2.childs.length) {
        if (c1 != w1 || c2 != w2)
          findDoupling(tree, c1, c2)
        c2 = w2.childs[j2];
        j2++;
      }
      c1 = w1.childs[j1];
      j1++;
    }
  }
}

collapseTree = (tree) => {
  if (tree instanceof WordOption) {
    collapseTree(tree.word)
  }
  for (let w of tree.childs) {
    collapseTree(w);
    if (w.childs.length == 1 && !(w instanceof WordOption) && !(w.childs[0] instanceof WordOption)) {
      w.childs[0].word = w.word + " " + w.childs[0].word;
      tree.removeChild(w);
      tree.addChild(w.childs[0]);
    }
  }
}

fs.writeFileSync("./tree.json", JSON.stringify(tree.print(), null, 4));

// combinePaths(tree, 1);
collapseTree(tree);

fs.writeFileSync("./tree_c.json", JSON.stringify(tree.print(), null, 4));
