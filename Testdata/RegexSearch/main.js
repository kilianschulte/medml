let fs = require("fs")
let _ = require("lodash")
let data = require("../befunde.json");

function run() {

  let results = [];
  for (let d of data) {
    if (!d[2]) continue;
    let sentences = d[2].match(/([^\.!\?]+[\.!\?]+)|([^\.!\?]+$)/g).reduce((a, s) => {
      if (a.length > 0 && (
        s.trim().charAt(0) === s.trim().charAt(0).toLowerCase() ||
        a[a.length-1].match(/([\( ]ca| Std| bzw| Dr| ggf| o(\.g)?|^V(\.a)?| postop| intrapulm| a(\.e)?|^Z(\. n)?| inkl)\.$/)
      )) {
        a[a.length-1] += s;
        return a;
      } else {
        return [...a, s.trim()];
      }
    }, []);
    results.push({
      id: d[0],
      text: sentences,
      befunde: _.merge({}, ...sentences.map(analyse))
    })
  }

  fs.writeFileSync("./regex.json", JSON.stringify(results, null, 4));

}

function analyse(s) {

  let result = {}

  if (s.match(/Die links?ventrikuläre Auswurffraktion/)) {
    result["linksventrikuläre Auswurffraktion"] = matchLeftFraction(s);
  }
  if (s.match(/Die linksventrikuläre Kammer/)) {
    result["linksventrikuläre Kammer"] = matchLeftChamber(s);
  }
  let m1 = s.match(/Die ((Ruhe-? und )?Belastungsaufnahmen?|(Belastungs-? und )?Ruheaufnahmen?) (zeigen?|zeigt)/)
  let m2 = s.match(/In (Ruhe(-? und Belastung)?|de[nr] (Ruhe-? und )?Belastungsaufnahmen?|de[nr] (Belastungs-? und )?Ruheaufnahmen?) (zeigt sich|findet sich)/)
  if (m1 || m2) {
    let comb = (m1 ? m1[0] : "") + (m2 ? m2[0] : "");
    result["Aufnahme"] = {
      "Ruhe": comb.includes("Ruhe"),
      "Belastung": comb.includes("Belastung"),
      "Resultat": matchImage(s)
    }
  }

  if (s.match(/Keine regionalen? Wandbewegungs?störung(en)?/)) {
    result["regionale Wandbewegungsstörung"] = false;
  }

  if (s.match(/Morphologische Korrelation/)) {
    result["Morphologische Korrelation"] = matchMorphCorrelation(s);
  }


  return result;

}

function matchMorphCorrelation(s) {

  let matching = [
    s.match(/\(.?low[- ]dose[- ]CT.? ?(zur Schwächungskorrektur)?(zum (?:Calcium|Kalzium)[ -]?[Ss]coring)?[;,]? ersetzt keine diagnostische CT\)/),
    s.match(/[Dd]egenerative (Veränderung(?:en)? der )?(Lenden)?[Ww]irbelsäule/),
    s.match(/In den mit ?abgebildeten Lungenabschnitten (?:findet sich )?(.*)/),
    s.match(/[Kk]eine? (?:Nachweis )?suspekte[rn] (intrapulmonaler )?(Lungen)?[Rr]undherde?/),
    s.match(/[Kk]ein Erguss/),
    s.match(/[Kk]eine? Infiltrate?/),
    s.match(/Subpleurale (und interlobassoziierte )?Verdichtung (.*)/),
    s.match(/((?:Aorten|Aortenklappen|Koronar)-?,? ?(?:und )?)+sklerose(?:,? (?:betont )?im Bereich der ((?:LAD|RCA)(?: und )?)+)?/)
  ]

  let l = matching.filter(m => m !== null).length;
  if (l == 0) {
    debugger;
  } else {

    let result = {};

    if (matching[0]) {
      result["lowDoseCT"] = matching[0][1] ? "Schwächungskorrektur" : matching[0][2] ? "Kalziumscoring" : true;
    } else {
      result["lowDoseCT"] = false;
    }

    if (l == 1 && matching[0]) {
      let rest = s.match(/diagnostische CT\):? (.*)/);
      if (rest) result["text"] = rest[1];
    }

    result["degenerative Wirbelsäule"] = bool(matching[1])
    if (matching[2]) {
      result["Lungenabschnitte"] = matching[2][1];
    }
    result["Lungenrundherde"] = bool(matching[3]) ? false : null
    result["Erguss"] = bool(matching[4]) ? false : null
    result["Infiltrate"] = bool(matching[5]) ? false : null
    if (matching[6]) {
      result["subpleurale Verdichtung"] = {
        interlobassoziiert: bool(matching[6][1]),
        details: matching[6][2]
      }
    }
    if (matching[7]) {
      let filterMatches = (str) => ({reg}) => reg.test(str);
      result["Sklerose"] = {
        typ: [
          { name: "Aorten", reg: /Aorten[-, s]/ },
          { name: "Aortenklappen", reg: /Aortenklappen/ },
          { name: "Koronar", reg: /Koronar/ }
        ].filter(filterMatches(matching[7][0])).map(({name}) => name),
        bereich: ["LAD", "RCA"].filter(a => matching[7][0].includes(a))
      }
    } else {
      result["Sklerose"] = false;
    }

    return result;
  }

}

function matchImage(s) {

  let matching = [
    s.match(/eine (.*?)?\s?(M|Tracerm)inderbelegung/),
    s.match(/eine (unauffällige|fleckige) Tracerbelegung (?:im|des) (linksventrikulären Myokards?)/)
  ]

  if (matching.filter(m => m !== null).length == 0) {
    let rest = s.match(/(?:aufnahmen? zeigen?|aufnahmen? zeigt|zeigt sich|findet sich) (.*)/);
    if (rest) return rest[1];
    else return s
  } else {
    if (matching[0]) {
      let match = [
        s.match(/(?:im Bereich der|in den|der) (basisnahen|distralen|apikalen|spitzennahen) (?:2\/3|zwei Dritteln)/),
        s.match(/der (.*?) (Kammer|Lateral)wand/),
        s.match(/anteriolateral/),
      ]

      let kattrib = "";

      if (match[1]) {
        if (match[1][1].includes("der")) {
          kattrib = match[1][1].split("der").map(m => m.trim()).reduceRight((full, split) => {
            if (full == "") {
              return split;
            } else if (split.endsWith("sowie") || split.endsWith("und")) {
              return split+" "+full;
            } else {
              return full;
            }
          })
        } else {
          kattrib = match[1][1];
        }
      }

      return  {
        typ: "Tracerbelegung",
        minderbelegung: true,
        attribute: matching[0][1] ? matching[0][1].split(/[\s,]/).filter(a => a && ["und", "weitgehend", "bis"].indexOf(a) == -1) : [],
        bereich: match[0] ? match[0][1] + " 2/3" : "gesamt",
        kammerwand: [
          match[2],
          match[1] ? match[1][2] == "Lateral" ? "lateral" : null : null,
          ...kattrib.split(/[\s,]/).filter(a => a && ["und", "sowie"].indexOf(a) == -1)
        ].filter(a => a && true)
      }
    } else if (matching[1]) {
      return {
        typ: "Tracerbelegung",
        minderbelegung: false,
        attribut: matching[1][1],
        ort: matching[1][2]
      }
    }
  }

}

function matchLeftChamber(s) {

  let a = s.match(/Kammer ist (nach Belastung )?((?:sowie )?in Ruhe )?(gering|mäßiggradig|mittelgradig|deutlich|.*) dilatiert/)

  if (!a) {
    return s.match(/Die linksventrikuläre Kammer (.*)/)[1];
  } else {

    let grad = ["gering", "mäßiggradig", "mittelgradig", "deutlich"].indexOf(a[3]);
    if (grad == -1)
      grad = a[3]
    else
      grad++;

    return {
      "Belastung": bool(a[1]),
      "Ruhe": bool(a[2]),
      "Dilaterationsgrad": grad
    }

  }

}

function matchLeftFraction(s) {

    let as = s.match(/(nach|unter|vor) Belastung (?:beträgt|ist)?\s*(>)?\s*(\d*)\s*%|(\d*)\s*% (nach|unter|vor) Belastung/g);
    let b = s.match(/in Ruhe (\(?nach Belastung\)?)?\s*(?:beträgt|ist)?\s*(>)?\s*(\d*)\s*\(?(\d*)?\)?\s*%|(\d*)\s*% in Ruhe\s*(\(?nach Belastung\)?)?/);
    let c = s.match(/in Ruhe und Belastung (?:beträgt|ist)?\s*(>)?\s*(\d*)\s*%/);
    let d = s.match(/nach Belastung und in Ruhe (mehr als|>)?\s*(\d*)\s*%/);

    if (!as && !b && !c && !d) {
      return s.match(/Die linksventrikuläre Auswurffraktion (.*)/)[1];
    } else {
      let frac = {};
      let makeB = (gt, amount) => {
        frac["unter Belastung"] = {
          mehrAls: bool(gt),
          prozent: parseInt(amount)
        }
      }
      let makeR = (stress, gt, amount) => {
        if (stress)
          frac["Ruhe nach Belastung"] = {
            mehrAls: bool(gt),
            prozent: parseInt(amount)
          }
        else
          frac["Ruhe"] = {
            mehrAls: bool(gt),
            prozent: parseInt(amount)
          }
      }
      if (as) {
        for (let ma of as) {
          let a = ma.match(/(nach|unter|vor) Belastung (?:beträgt|ist)?\s*(>)?\s*(\d*)\s*%|(\d*)\s*%,? (nach|unter|vor) Belastung/);
          if ((a[1] || a[5]) === "unter")
            makeB(a[2], a[3] || a[4]);
          else
            makeR((a[1] || a[5]) === "nach", a[2], a[3] || a[4])
        }
      }
      if (b) {
        if (b[1] && b[4]) {
          makeR(true, false, b[4])
          makeR(false, b[2], b[3])
        } else {
          makeR(b[1] || b[6], b[2], b[3] || b[5])
        }
      }
      if (c) {
        makeB(c[1], c[2]);
        makeR(false, c[1], c[2]);
      }
      if (d) {
        makeR(true, d[1], d[2]);
        makeR(false, d[1], d[2]);
      }

      return frac;
    }
  }

setTimeout(run, 5000);

function bool(v) {
  return (v && true) || false;
}
