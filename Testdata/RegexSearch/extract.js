let fs = require("fs")
let _ = require("lodash")
let data = require("../befunde.json");

function run() {

  let results = [];
  for (let d of data) {
    if (!d[2]) continue;
    let text = d.slice(2).join(" ");
    results.push({
      id: d[0],
      text,
      befunde: analyse(text)
    })
  }

  fs.writeFileSync("./extract.json", JSON.stringify(results, null, 4));

}

function analyse(s) {

  let result = {}

  result["Auswurffraktion"] = matchLeftFraction(s);
  result["Segmente"] = matchSegments(s);
  result["Kalk-Score"] = matchScore(s);


  return result;

}

function matchScore(s) {

/*

Unauffälliger Kalk-Score (Agatston Score): 0,6;
Unauffälliger Kalk-Score (Agatston Score): 16;
Unauffälliger Kalk-Score (Agatston Score): 29;
Massiv erhöhter Kalk-Score (Agatston Score): 3011;
UnauffälligerErhöhter Kalk-Score (Agatston Score): 0;
Unauffälliger Kalk-Score (Agatston Score): 46;
Erhöhter Kalk-Score (Agatston Score): 1184;
Unauffälliger Kalk-Score (Agatston Score): 505;
Nicht pathologisch erhöhter Kalk-Score (Agatston Score):160;
Unauffälliger Kalk-Score (Agatston Score): 9,1;
Deutlich erhöhter Kalk-Score (Agatston Score): 1406,6;
Erhöhter Kalk-Score (Agatston Score): 546;
Unauffälliger Kalk-Score (Agatston Score): 23,3;
Gering erhöhter Kalk-Score (Agatston Score): 95;
 */

let a = s.match(/(?:Kalk-Score\s*(?:\(Agatston Score\))?|(?:Kalk-Score)?\s*\(Agatston Score\)):?\s*(?:>\s*)?(?:von\s*)?(\d+,?\d?);?|(\d+,?\d?) \(Agatston Score\)/);
return a != null ? a[1] != null ? parseFloat(a[1]) : parseFloat(a[2]) : null;

}

function matchVolumina(s) {
  /*
   (linksventrikuläre diastolische Volumen nach Belastung 74 ml, in Ruhe  64 ml)
   post-Stress Dilatation des enddiastolischen Volumens von 101 ml auf 128 ml
   Deutliche post-Stress Dilatation des enddiastolischen Volumens von 121 ml auf 144 ml
   enddiastolischen Volumen von 163 ml und 170 ml nach Belastung
   Zunahme des enddiastolisches Volumen des linken Ventrikels von 108 ml in Ruhe auf 134 ml nach Belastung
   erhöhtes enddiastolisches Volumen (EDV in Ruhe 145ml, bei Belastung 152 ml)
   Erhöhte enddiastolische Volumina (140ml bei Belastung, 143 ml in Ruhe)
   erhöhten endiastolischen Volumina (In Ruhe 163ml; Unter Belastung 155ml)
   Enddiastolisches Volumen in Ruhe 121 ml, unter Belastung 142 ml

   Das enddiastolische Volumen beträgt in Ruhe 118 ml und nach Belastung 120 ml.
   Das Enddiastolishe Volumen beträgt in Ruhe 175ml und unter Belastung 181ml
   Das enddiastolisches Volumen beträgt in Ruhe 117 ml und 138 ml nach Belastung
   Das Enddiastolische Volumen beträgt in Ruhe 144 ml, nach Belastung 138 ml
   Das Enddiastolische Volumen beträgt in Ruhe 92 ml, nach Belastung 104 ml
   Das enddiastolische Volumen beträgt in Ruhe 118 ml und nach Belastung 120 ml
   */
}

function matchSegments(s) {
/*
  Unter körperlicher Ausbelastung findet sich eine umschriebene Tracerminderbelegung in der basisnahen Hälfte der inferolateralen Kammerwand vereinbar mit einer Perfusionsstörung in diesem Bereich (1-2 Segmente/17; DD Schwächungsartefakt)
  Nachweis einer ausgedehnten nichttransmuralen  Narbe in der anteroseptalen Kammerwand (3 Segmente).
  Unter Regadenoson-Belastung ausgedehnte belastungsinduzierte Randischämie in der mittventrikulären bis apexnahen anteroseptalen Kammerwand, a.e. LAD-Versorgungsgebiet (3 Segmente).
  Zudem zeigte sich inferoseptal eine belastungsinduzierte Perfusionsstörung (1 Segment), a.e. im RCA Versorgungsgebiet
  In den Belastungsaufnahmen zeigt sich eine verminderte Tracerbelegung in der Apex, in der apikalen Hälfte der Vorderwand und des Septums (ca. 4-5 Segmente)
  In den Belastungsaufnahmen zudem  verminderte Tracerbelegung im apikalen Drittel der Hinterwand (ca. 1-2 Segmente)
  Nachweis belastungsinduzierter Perfusionsstörungen in der Apex, in der apikalen Hälfte der Vorderwand und des Septums (ca. 4-5 Segmente) a.e. LAD-Gebiert sowie  im apikalen Drittel der Hinterwand (ca. 1-2 Segmente), a.e. RCA-Gebiet
  Nachweis einer mäßigen belastungsinduzierten Perfusionsstörung in der apikalen Hälfte der Vorderwand (ca.3-4 / 17 Segmente)
  Die Belastungsaufnahmen zeigen eine Minderbelegung in der apikalen inferolateralen Kammerwand (ca. 1-2 / 17 Segmente)
  Es findet sich lediglich eine geringgradige belastungsinduzierte Minderperfusion in der apikalen inferolateralen Kammerwand (ca. 1-2/17 Segmente)
  Die Ruhe- und Belastungsaufnahmen zeigen eine Minderbelegung der gesamten inferolateralen Kammerwand (ca. 4-5/17 Segmenten)
  Nachweis einer nicht vollständig transmuralen Narbe in der gesamten inferolateralen Kammerwand  (ca. 4-5/17 Segmenten) mit geringer Randischämie
  Unter Regadenoson-Belastung zeigte sich eine mäßige belastungsinduzierte Perfusionsstörung in der apikalen Hälfte der inferolateralen Kammerwand (3-4 Segmente)
  Umschriebene belastungsinduzierte Ischämie mittventrikulär anteroseptal (1-2 / 17 Segmente) sowie basal inferoseptal (1/17 Segmente)
  Unter Adenosin-Belastung zeigt sich eine belastungsinduzierte Ranzonenischämie in der apexnahen Hälfte der anterioren linksventrikulären Kammerwand (2-3 Segmente/17) angrenzend an die Narbe im anterioren Drittel der Vorderwand, des Apex sowie des anterioren Drittels der inferolateralen Kammerwand
*/

  let regex1 = new RegExp("\\((?:ca.)?\\s*(\\d\\d?)-?(\\d\\d?)?\\s*(?:\\/\\s*17\\s*)?Segmente?n?(?:\\s*\\/\\s*17)?(?:;[^\\)]*)?\\)|in (\\d\\d?) von 17 Segmenten?|\\(je (\\d\\d?) Segmente?n?\\)", "g");
  let a, res = [];

  while ((a = regex1.exec(s)) !== null) {
    let num = a[1] != null
        ? a[2] != null
          ? (parseInt(a[1])+parseInt(a[2]))/2
          : parseInt(a[1])
        : a[3] != null
          ? parseInt(a[3])
          : parseInt(a[4]) * 2;
    res.push(num);
  }
  return res.length > 0 ? res.reduce((sum, n) => sum + n, 0) : null;
}

String.prototype.matchAll = function(regex) {
  let res, arr = [];
  while ((res = regex.exec(this)) !== null) {
    arr.push(res);
  }
  return arr;
}

function matchLeftFraction(s) {

    let as = s.match(/(nach|unter|vor) Belastung (?:beträgt|ist)?\s*>?\s*(\d*)\s*%|(\d*)\s*% (nach|unter|vor) Belastung/g);
    let b = s.match(/in Ruhe (\(?nach Belastung\)?)?\s*(?:beträgt|ist)?\s*>?\s*(\d*)\s*\(?(\d*)?\)?\s*%|(\d*)\s*% in Ruhe\s*(\(?nach Belastung\)?)?/);
    let c = s.match(/in Ruhe und Belastung (?:beträgt|ist)?\s*>?\s*(\d*)\s*%/);
    let d = s.match(/nach Belastung (?:und|sowie) in Ruhe (?:mehr als|>)?\s*(\d*)\s*%/);

    if (!as && !b && !c && !d) {
      if (s.match(/links?ventrikuläre Auswurffraktion/))
        return s.match(/links?ventrikuläre Auswurffraktion (.*)/)[1];
      else return null;
    } else {
      let frac = {};
      let makeB = (gt, amount) => {
        frac["unter Belastung"] = {
          mehrAls: bool(gt),
          prozent: parseInt(amount)
        }
      }
      let makeR = (stress, gt, amount) => {
        if (stress)
          frac["Ruhe nach Belastung"] = {
            mehrAls: bool(gt),
            prozent: parseInt(amount)
          }
        else
          frac["Ruhe"] = {
            mehrAls: bool(gt),
            prozent: parseInt(amount)
          }
      }
      if (as) {
        for (let ma of as) {
          let a = ma.match(/(nach|unter|vor) Belastung (?:beträgt|ist)?\s*(>)?\s*(\d*)\s*%|(\d*)\s*%,? (nach|unter|vor) Belastung/);
          if ((a[1] || a[5]) === "unter")
            makeB(a[2], a[3] || a[4]);
          else
            makeR((a[1] || a[5]) === "nach", a[2], a[3] || a[4])
        }
      }
      if (b) {
        if (b[1] && b[4]) {
          makeR(true, false, b[4])
          makeR(false, b[2], b[3])
        } else {
          makeR(b[1] || b[6], b[2], b[3] || b[5])
        }
      }
      if (c) {
        makeB(c[1], c[2]);
        makeR(false, c[1], c[2]);
      }
      if (d) {
        makeR(true, d[1], d[2]);
        makeR(false, d[1], d[2]);
      }

      return frac;
    }
  }

run();

function bool(v) {
  return (v && true) || false;
}
