const fs = require("fs");

let data = fs.readFileSync("../befunde_herz_anon.txt", "latin1");

data = data
  .split("\n") //split lines
  .map(d => d
    .split("|") //split values
    .map(v => '"'+v.trim().replace(/"/g, '""')+'"') //format values (escape double quotes)
    .join(",") //join values
  )
  .join("\n"); //join lines

fs.writeFileSync("../befunde.csv", data);
