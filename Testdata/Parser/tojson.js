const fs = require("fs");

let data = fs.readFileSync("../befunde_herz_anon.txt", "latin1");

data = data
  .split("\n") //split lines
  .map(d => d
    .split("|") //split values
    .map(v => v.trim()) //format values (escape double quotes)
  )

fs.writeFileSync("../befunde.json", JSON.stringify(data, null, 4));
