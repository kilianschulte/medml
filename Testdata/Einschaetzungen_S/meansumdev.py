# -*- coding: utf-8 -*-
"""
Created on Mon May 27 16:02:49 2019

@author: wiifr
"""

import pandas as pd
from pandas.io.json import json_normalize

import numpy as np
from sklearn import linear_model,preprocessing, model_selection
from sklearn.metrics import r2_score
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import json
from matplotlib import style
style.use('ggplot')

def weighted():
    data = json.load(open('classification.json'))
    temp = pd.DataFrame.from_dict([data],orient='columns').T
    df = pd.DataFrame()
    for i in range(0,temp.shape[0]-1):
        curr = temp.values[i]
        curr = json_normalize(curr)
        df = df.append(curr,sort=True)
    labels = np.array(df['id'])
    
    
    printtop(df,3,'result.mean.weighted')
    printtop(df,3,'result.deviation.weighted')
    
    yplot = np.array(df['result.mean.weighted'])
    xplot = np.linspace(1,0,yplot.size)
    plt.scatter(xplot,yplot)
    #showlabels(labels,xplot,yplot)
    
    plt.show()
    
def printtop(df,nr,col):
    print('top ' , nr, ' of ', col,': ')
    df =df.sort_values(col,0)
    df['abs']=[abs(x) for x in df[col]]
    df2 = df.sort_values('abs')
    
    good = []
    medium = []
    bad = []
    for i in range(nr):
        bad.append(df.iloc[i]['id'])
        good.append(df.iloc[-(i+1)]['id'])
        medium.append(df2.iloc[i]['id'])

    print('good:',good)
    print('medium: ',medium)
    print('bad: ',bad)

#    
def showlabels(labels,xplot,yplot):
    for i in range(len(xplot)):
        label=labels[i]
        x=xplot[i]
        y=yplot[i]
        plt.annotate(label, # this is the text
                 (x,y), # this is the point to label
                 textcoords="offset points", # how to position the text
                 xytext=(0,10), # distance from text to points (x,y)
                 ha='center') # horizontal alignment can be left, right or center
    

weighted()