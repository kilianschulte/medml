# -*- coding: utf-8 -*-
"""
Created on Thu May 23 13:54:15 2019

@author: wiifr
"""

import pandas as pd
import numpy as np
from sklearn import linear_model,preprocessing, model_selection
from sklearn.metrics import r2_score
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')

labelsVisible = True

def train():#trainingsdatensatz einfuegen
    df = pd.read_csv('trainingsdatensatz.csv',sep='\s*,\s*', engine='python')
    df=df.drop(['id','confidence_normal','confidence_abnormal'],1)
    X = np.array(df.drop('rating',1)) #features
    y= np.array(df['rating']) #labels
    
#=================train linear model=================
    reg = linear_model.Ridge()
    reg.fit(X,y.ravel()) 
    reg_acc = reg.score(X,y) #test with testing data
    print('accuracy regression: ','%.2f'%(reg_acc*100),'%')
    
#==================train Neural Network=============
    nn_acc=0.0
    while(nn_acc<0.8):
        nn = MLPRegressor(hidden_layer_sizes=(3), 
                  activation='tanh', solver='lbfgs')
        n = nn.fit(X, y.ravel())
        nn_acc = n.score(X,y) #test with testing data
    print('accuracy Neural Network: ','%.2f'%(nn_acc*100),'%')

    return reg,n
    
def rate(model,nr):
    df = pd.read_csv('classification.csv',sep='\s*,\s*',header=0, encoding='ascii', engine='python')
    confab = np.array(df['confidence_abnormal'])
    df=df.drop(['confidence_normal','confidence_abnormal'],1)
 
    labels = df.iloc[:,0]
    labels.fillna(00000000,inplace=True)
    labels = [str(y) for y in [int(x) for x in labels]]
    
    df = df.drop('id',1)
    #df = df.iloc[:,1:] 
    #df.columns=['good','medium','bad']

    X=np.array(df)
    
    ratings = []
    for i in range(len(X)):
        predict_me = np.array(X[i].astype(float))
        predict_me = predict_me.reshape(-1, len(predict_me))
        rating_val = round(model.predict(predict_me)[0],2)
        ratings.append(rating_val)
    
    #plot with ids
    plt.figure(nr);
    plotcolored(ratings,labels)
    #plotnormalized(ratings,confab)

    
def plotnormalized(input,confab):
    yplot = normalize(input)
    xplot = [i for i, val in enumerate(yplot)]
#    diffplot=[]
#    for i in range(len(xplot)):
#        diffplot.append(yplot[i]-confab[i])
    plt.plot(xplot,yplot,'b',xplot,confab,'r')
    plt.show()
def normalize(input):
    minimum = min(input)
    yplot = [x-minimum for x in input]
    maximum = max(yplot)
    yplot = [x/maximum for x in yplot]
    return yplot

def plotcolored(yplot,labels):
    bad = [i for i, val in enumerate(yplot) if val>0.25]
    medium = [i for i, val in enumerate(yplot) if val>=-0.25 and val<=0.25]
    good = [i for i, val in enumerate(yplot) if val<-0.25]
    
    plt.scatter(bad,[yplot[i] for i in bad],c='r')
    plt.scatter(medium,[yplot[i] for i in medium],c='y')
    plt.scatter(good,[yplot[i] for i in good],c='g')

    for i in range(len(bad)):
        label = labels[i]
        x= bad[i]
        y=[yplot[y] for y in bad][i]
        annot=plt.annotate(label, # this is the text
                 (x,y), # this is the point to label
                 textcoords="offset points", # how to position the text
                 xytext=(0,10), # distance from text to points (x,y)
                 ha='center') # horizontal alignment can be left, right or center
        annot.set_visible(labelsVisible)

    for i in range(len(good)):
        label = labels[i]
        x= good[i]
        y=[yplot[i] for i in good][i]
        annot=plt.annotate(label, # this is the text
                 (x,y), # this is the point to label
                 textcoords="offset points", # how to position the text
                 xytext=(0,10), # distance from text to points (x,y)
                 ha='center') # horizontal alignment can be left, right or center
        annot.set_visible(labelsVisible) 
    plt.show()
    
def printtop(df,nr,col):
    print('top ' , nr, ' of ', col,': ')
    df =df.sort_values(col,0)
    df['abs']=[abs(x) for x in df[col]]
    df2 = df.sort_values('abs')
    
    good = []
    medium = []
    bad = []
    for i in range(nr):
        bad.append(df.iloc[i]['id'])
        good.append(df.iloc[-(i+1)]['id'])
        medium.append(df2.iloc[i]['id'])

    print('good:',good)
    print('medium: ',medium)
    print('bad: ',bad)
#test()
reg,nn = train();
rate(reg,1)
rate(nn,2)
