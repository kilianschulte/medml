# -*- coding: utf-8 -*-
"""
Created on Mon May 27 13:52:01 2019

@author: wiifr
"""
import pandas as pd
import numpy as np
from sklearn import linear_model,preprocessing, model_selection
from sklearn.metrics import r2_score
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')

def test():
    df = pd.read_excel('Auswertung_Einschaetzung.xlsx')
    #df = excel.parse('IDs')
    df = df.iloc[:,2:6]
    df.columns=['trueVal','good','medium','bad']
    #print(df)
#===================== Machine Learning =========================================
    X = np.array(df.drop('trueVal',1)) #features
    y= np.array(df['trueVal']) #labels
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X,y,test_size=0.2)

    reg = linear_model.Ridge(alpha=.1)
    reg.fit(X_train, y_train.ravel()) #train with training data

    nn = MLPRegressor(hidden_layer_sizes=(3), 
                  activation='tanh', solver='lbfgs')
    n = nn.fit(X_train, y_train.ravel())

#===================== Direct Solutions===========================================
    lin_arr = [] #linear regression
    nn_arr = [] #Neural Network
    mean_arr = [] #mean value
    sum_arr = [] #weighted sums & thresholds
    deviation_arr = [] #good: +1, bad: -1, medium: nearer to zero
    for i in range(len(X)):
        predict_me = np.array(X[i].astype(float))
        predict_me = predict_me.reshape(-1, len(predict_me))
        lin_pred = int(round(reg.predict(predict_me)[0],0))
        lin_arr.append(lin_pred)
        
        nn_pred = int(round(n.predict(predict_me)[0],0))
        nn_arr.append(nn_pred)
        
        
        mean = int(round((X[i,0]+X[i,1]*2+X[i,2]*3)/(X[i,0]+X[i,1]+X[i,2]),0))
        mean_arr.append(mean)
        
        val = round((X[i,0]+X[i,1]*2+X[i,2]*3),2) #exponential
        if val>70:
            sumval = 3
        elif val>45:
            sumval = 2
        else:
            sumval = 1
        sum_arr.append(sumval)
        
        deviation = X[i,0]-X[i,2]
        if X[i,1]!=0:
            deviation = deviation/X[i,1]
        if deviation<-0.99:
            deviation=3
        elif deviation>0.99:
            deviation=1
        else:
            deviation=2
        deviation_arr.append(round(deviation,2))
            
        
    df['regr.']=lin_arr
    df['NN']=nn_arr
    df['mean']=mean_arr
    df['sum']=sum_arr
    df['dev.']=deviation_arr
    print(df[['trueVal','NN','regr.','mean','sum','dev.','good','medium','bad']])
    
    reg_acc = reg.score(X_test,y_test) #test with testing data
    print('accuracy regression: ','%.2f'%(reg_acc*100),'%')
    
    nn_acc = n.score(X_test,y_test) #test with testing data
    print('accuracy Neural Network: ','%.2f'%(nn_acc*100),'%')
    
    nn_acc2 = r2_score(df['trueVal'],df['NN'])
    print('accuracy Neural Network int vals: ','%.2f'%(nn_acc2*100),'%')

    mean_acc = r2_score(df['trueVal'],df['mean'])
    print('accuracy mean: ','%.2f'%(mean_acc*100),'%')
    
    sum_acc =  r2_score(df['trueVal'],df['sum'])
    print('accuracy sum: ','%.2f'%(sum_acc*100),'%')
    
    deviation_acc =  r2_score(df['trueVal'],df['dev.'])
    print('accuracy dev: ','%.2f'%(deviation_acc*100),'%')

test()