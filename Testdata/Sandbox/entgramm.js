
const fs = require("fs")
const data = require("./einsch.json");
const config = require("./entgramm.json")
const _ = require("lodash")

let result = {};

result.gut = entgramm(data.gut);
result.mittel = entgramm(data.mittel);
result.schlecht = entgramm(data.schlecht);

function entgramm(list) {
  let simple = list
    // .reduce((l, s) => [...l, ...s.split(/[\.\?!;]/g)], [])
    .map(s => s.trim().toLowerCase())
    .filter(s => s.length > 0)
    .map(s => s.replace(/ä/g, "ae").replace(/ö/g, "oe").replace(/ü/g, "ue").replace(/ß/g, "ss"))
    .map(s => s.replace(/[\."'`]/g, "").replace(/[^\s\w\d]/g, (x)=>" "+x+" ").replace(/[;,-]|\(|\)/g, "").replace(/\s+/g, " ").trim())
    .map(s => s.split(" "))

  for (let item of config.unify) {
    let regex = new RegExp(`^(${item.allowStemOnly ? '\\S*' : '\\S+'}${item.stem})(?:${item.suffix.join("|")})?$`);
    simple = simple.map(s => s.map(w => {
      let m = w.match(regex);
      if (!m) return w;
      return m[1];
    }))
  }

  simple = simple.map(s => s.filter(w => config.clear.indexOf(w) == -1)).map(s => s.join(" "));

  return simple

}

function countSingle(list) {
  let result = {}
  list.map(s => s.split(" ").forEach(w => {
    if (result[w]) result[w]++;
    else result[w] = 1;
  }));
  return result;
}

function countWords(list, num) {
  let result = {}
  list.map(s => s.split(" ").reduce((wOut, _, i, wIn) => {
    if (i >= wIn.length-num) return wOut;
    return [...wOut, wIn.slice(i, i+num).join(" ")];
  }, []).forEach(w => {
    if (result[w]) result[w]++;
    else result[w] = 1;
  }));
  return result;
}

function allWordsUsed(cla, words) {
  let claWords = _.flatten(cla.map(c => c.word.split(" ")));
  return words.every(w => claWords.indexOf(w) !== -1);
}

let words = _.uniq([
  ..._.flatten(result.gut.map(s => s.split(" "))),
  ..._.flatten(result.mittel.map(s => s.split(" "))),
  ..._.flatten(result.schlecht.map(s => s.split(" "))),
]);

console.log(words.length);

let classification = [];
let n = 0;

while (!allWordsUsed(classification, words) && n < 40) {

  let count = {};
  count.gut = countWords(result.gut, n+1);
  count.mittel = countWords(result.mittel, n+1);
  count.schlecht = countWords(result.schlecht, n+1);

  let keys = _.uniq([...Object.keys(count.gut), ...Object.keys(count.mittel), ...Object.keys(count.schlecht)]);

  let classifi = [];

  for (let w of keys) {
    let o = {
      word: w,
      gut: count.gut[w] || 0,
      mittel: count.mittel[w] || 0,
      schlecht: count.schlecht[w] || 0
    }

    let i = (o.gut == 0) + (o.mittel == 0) + (o.schlecht == 0);
    if (i == 2) {
      classifi.push(o);
    }
  }

  // MERGE STRATEGY
  const merge = 2;
  if (merge == 0) {
  // at least on unused word in new phrase
  classifi = classifi.filter(c => c.word.split(" ").some(w => classification.every(c => !c.word.includes(w))));
} else if (merge == 1) {
  // 1 word shorter phrase (start / end) never used before
  classifi = classifi.filter(c => {
    let words = c.word.split(" ");
    let used = (p) => classification.some(c => c.word == p);
    return !used(words.slice(1).join(" ")) && !used(words.slice(0, words.length-1).join(" "))
  })
} else if (merge == 2) {
  // 1 word shorter phrase never used before OR phrase has same count as shorter phrase
  // -> priorizize more specific phrases
  classifi = classifi.filter(c => {
    let words = c.word.split(" ");
    let findUsed = (p) => classification.find(c => c.word == p);
    let count = (o) => o.gut+o.mittel+o.schlecht;

    let pre = findUsed(words.slice(1).join(" "));
    let post = findUsed(words.slice(0, words.length-1).join(" "));

    if (pre && post) {
      if (count(pre)+count(post) == count(c)) {
        classification.splice(classification.indexOf(pre), 1);
        classification.splice(classification.indexOf(post), 1);
        return true;
      } else {
        return false;
      }
    } else if (pre) {
      if (count(pre) == count(c)) {
        classification.splice(classification.indexOf(pre), 1);
        return true;
      } else {
        return false;
      }
    } else if (post) {
      if (count(post) == count(c)) {
        classification.splice(classification.indexOf(post), 1);
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  })

}
  classification = _.concat(classification, classifi);
  console.log(n, classifi.length, classification.length);

  n++;
}
classification = classification.filter(c => c.word.split(" ").some(w => w.length > 3) && c.word.split(" ").every(w => !parseInt(w)));

fs.writeFileSync("./entgramm_result.json", JSON.stringify(result, null, 4));
fs.writeFileSync("./word_classifi.json", JSON.stringify(classification, null, 4));
