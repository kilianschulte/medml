
const fs = require("fs");
const config = require("./entgramm.json")
const words = require("./word_classifi.json");

let befunde = require("../befunde.json");
console.log(befunde.length);

function entgramm(list) {
  let simple = list
    // .reduce((l, s) => [...l, ...s.split(/[\.\?!;]/g)], [])
    .map(s => s.trim().toLowerCase())
    .filter(s => s.length > 0)
    .map(s => s.replace(/ä/g, "ae").replace(/ö/g, "oe").replace(/ü/g, "ue").replace(/ß/g, "ss"))
    .map(s => s.replace(/[\."'`]/g, "").replace(/[^\s\w\d]/g, (x)=>" "+x+" ").replace(/[;,-]|\(|\)/g, "").replace(/\s+/g, " ").trim())
    .map(s => s.split(" "))

  for (let item of config.unify) {
    let regex = new RegExp(`^(${item.allowStemOnly ? '\\S*' : '\\S+'}${item.stem})(?:${item.suffix.join("|")})?$`);
    simple = simple.map(s => s.map(w => {
      let m = w.match(regex);
      if (!m) return w;
      return m[1];
    }))
  }

  simple = simple.map(s => s.filter(w => config.clear.indexOf(w) == -1)).map(s => s.join(" "));

  return simple

}

let result = befunde.map(b => {
  let original = b.slice(2, 5).join(" ");
  let str = entgramm(b.slice(2, 5)).join(" ");
  let included = words.filter(w => str.includes(w.word));

  let count = (k) => (a, i) => a + (i[k] > 0);
  let sum = (k) => (a, i) => a + i[k];
  let weight = (k) => (a, i) => a + (i[k] > 0) * i.word.split(" ").length;
  let sumweight = (k) => (a, i) => a + i[k] * i.word.split(" ").length;

  let f = (k, x) => included.reduce(x(k), 0);

  let bclass = {
    id: parseInt(b[0]),
    text: original,
    parsed: str,
    found: {
      gut: {
        words: included.filter(w => w.gut > 0).map(w => w.word+" -> "+w.gut),
        count: f("gut", count),
        sum: f("gut", sum),
        weighted: f("gut", weight),
        sumweighted: f("gut", sumweight)
      },
      mittel: {
        words: included.filter(w => w.mittel > 0).map(w => w.word+" -> "+w.mittel),
        count: f("mittel", count),
        sum: f("mittel", sum),
        weighted: f("mittel", weight),
        sumweighted: f("mittel", sumweight)
      },
      schlecht: {
        words: included.filter(w => w.schlecht > 0).map(w => w.word+" -> "+w.schlecht),
        count: f("schlecht", count),
        sum: f("schlecht", sum),
        weighted: f("schlecht", weight),
        sumweighted: f("schlecht", sumweight)
      }
    }
  }

  let mean = (k) => ((((bclass.found.gut[k]*1+bclass.found.mittel[k]*2+bclass.found.schlecht[k]*3)/((bclass.found.gut[k]+bclass.found.mittel[k]+bclass.found.schlecht[k]) || 1))-2)*-1)
  let deviation = (k) => ((bclass.found.gut[k]-bclass.found.schlecht[k])/Math.sqrt((bclass.found.mittel[k]+1)))

  let mittelIsNormal = 0.2;
  let schlechtIsNormal = -0.2;

  let weightedSum = (bclass.found.gut.sumweighted+bclass.found.mittel.sumweighted+bclass.found.schlecht.sumweighted);
  let confidenceNormal = ((bclass.found.gut.sumweighted+bclass.found.mittel.sumweighted*mittelIsNormal+bclass.found.schlecht.sumweighted*schlechtIsNormal)/weightedSum)
  let confidenceAbnormal = ((bclass.found.mittel.sumweighted*(1-mittelIsNormal)+bclass.found.schlecht.sumweighted*(1-schlechtIsNormal))/weightedSum);

  bclass.result = {
    mean: {
      count: mean("count"),
      sum: mean("sum"),
      weighted: mean("weighted"),
      sumweighted: mean("sumweighted")
    },
    deviation: {
      count: deviation("count"),
      sum: deviation("sum"),
      weighted: deviation("weighted"),
      sumweighted: deviation("sumweighted")
    },
    confidence: {
      normal: confidenceNormal,
      abnormal: confidenceAbnormal
    }
  }

  return bclass;
})

fs.writeFileSync("./classification.json", JSON.stringify(result, null, 4));
fs.writeFileSync("./classification.csv", [
  "id,mean_count, mean_sum, mean_weighted, mean_sumweighted, deviation_count, deviation_sum, deviation_weighted, deviation_sumweighted, confidence_normal, confidence_abnormal"
  ,...result.map(r => [
    r.id, r.result.mean.count, r.result.mean.sum, r.result.mean.weighted, r.result.mean.sumweighted, r.result.deviation.count, r.result.deviation.sum, r.result.deviation.weighted, r.result.deviation.sumweighted, r.result.confidence.normal, r.result.confidence.abnormal
  ].join(","))
].join("\n"));
