
const fs = require("fs");
const config = require("./entgramm.json")
const words = require("./word_classifi.json");

let befunde = require("../befunde.json");

function entgramm(list) {
  let simple = list
    // .reduce((l, s) => [...l, ...s.split(/[\.\?!;]/g)], [])
    .map(s => s.trim().toLowerCase())
    .filter(s => s.length > 0)
    .map(s => s.replace(/ä/g, "ae").replace(/ö/g, "oe").replace(/ü/g, "ue").replace(/ß/g, "ss"))
    .map(s => s.replace(/[\."'`]/g, "").replace(/[^\s\w\d]/g, (x)=>" "+x+" ").replace(/[;,-]|\(|\)/g, "").replace(/\s+/g, " ").trim())
    .map(s => s.split(" "))

  for (let item of config.unify) {
    let regex = new RegExp(`^(${item.allowStemOnly ? '\\S*' : '\\S+'}${item.stem})(?:${item.suffix.join("|")})?$`);
    simple = simple.map(s => s.map(w => {
      let m = w.match(regex);
      if (!m) return w;
      return m[1];
    }))
  }

  simple = simple.map(s => s.filter(w => config.clear.indexOf(w) == -1)).map(s => s.join(" "));

  return simple

}

const PRINT_IDS = ["1436313477", "3741030718"]

let result = befunde.map(b => {
  let str = entgramm(b.slice(2, 5)).join(" ");
  let included = words.filter(w => str.includes(w.word));

  if (PRINT_IDS.indexOf(b[0]) != -1) {
    console.log(b[0], ":\n", b.slice(2, 5), "\n", included.map(i => i.word+" -> +"+i.gut+" o"+i.mittel+" -"+i.schlecht))
  }

  let count = (k) => (a, i) => a + (i[k] > 0);
  let sum = (k) => (a, i) => a + i[k];
  let weight = (k) => (a, i) => a + (i[k] > 0) * i.word.split(" ").length;

  let f = (k, x) => included.reduce(x(k), 0);

  return [
    b[0],
    f("gut", count), f("mittel", count), f("schlecht", count),
    f("gut", sum), f("mittel", sum), f("schlecht", sum),
    f("gut", weight), f("mittel", weight), f("schlecht", weight)
  ]
})

let mean = (r, i) => ((((r[i]*1+r[i+1]*2+r[i+2]*3)/((r[i]+r[i+1]+r[i+2]) || 1))-2)*-1).toFixed(3);
let deviation = (r, i) => ((r[i]-r[i+2])/Math.sqrt((r[i+1]+1))).toFixed(3)

result = result.map(r => [...r, mean(r, 1), mean(r, 4), mean(r, 7), deviation(r, 1), deviation(r, 4), deviation(r, 7)])

result.unshift([
  "id",
  "gut_count", "mittel_count", "schlecht_count",
  "gut_sum", "mittel_sum", "schlecht_sum",
  "gut_weighted", "mitel_weighted", "schlecht_weighted",
  "mean_count", "mean_sum", "mean_weighted",
  "deviation_count", "deviation_sum", "deviation_weighted"
])

fs.writeFileSync("./classification.csv", result.map(r => r.join(",")).join("\n"));
