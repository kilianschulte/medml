

const fs = require("fs");

const classification = require("./classification.json")
  .filter(c => {
    let a = (c.result.mean.count>=0)+(c.result.mean.sum>=0)+(c.result.mean.weighted>=0)+(c.result.mean.sumweighted>=0);
    return a != 4 && a != 0;
  })
  .forEach(c => {
    console.log(c.id);
  })
