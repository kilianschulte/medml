const fs = require("fs");
const _ = require("lodash");

let data = require("./einsch.json");

class Node {
  constructor(w, c) {
    this.word = w;
    this.childs = c || [];
    this.childs.forEach(c => c.parent = this);
    this.parent = null;
    this.count = 0;
  }
  increaseCount() {
    this.count++;
  }
  setWord(w) {
    this.word = w;
  }
  addChild(c) {
    this.childs.push(c);
    c.parent = this;
  }
  removeChild(w) {
    if (w instanceof Node)
      this.childs = this.childs.filter(c => c !== w);
    else
      this.childs = this.childs.filter(c => c.word == w)
  }
  hasChild(w) {
    return this.getChild(w) && true;
  }
  hasChildNode(n) {
    return this.childs.find(c => c == n) && true
  }
  hasWord(w) {
    return w === this.word;
  }
  getChild(w) {
    return this.childs.find(c => c.hasWord(w))
  }
  equals(node) {
    if (this.word instanceof Node) {
      return this.word.equals(node.word)
    } else if (node.word instanceof Node ){
      return false;
    } else {
      return this.word == node.word;
    }
  }
  print() {

    let w = this.word;
    if (w instanceof Node)
      w = w.print();

    if (this.childs.length > 0) {
      return {
        word: w,
        count: this.count,
        childs: _.reverse(_.sortBy(this.childs.map(c => c.print()), "count"))
      }
    } else {
      return {
        word: w,
        count: this.count
      };
    }

  }
}

class Word extends Node {
  constructor(w) {
    super(w, null);
  }
}

class WordOption extends Node {
  constructor(options, childs) {
    super(new Node(null, options), childs);
  }
  hasWord(w) {
    return this.word.hasChild(w)
  }
  equals(node) {
    for (let w of this.word.childs) {
      if (!node.hasWord(w)) {
        return false;
      }
    }
    return true;
  }
  print() {
    debugger;
    return {
      word: this.word.childs.map(c => c.print()),
      childs: this.childs.map(c => c.print())
    }
  }
}

function createTree(list) {
  let tree = new Word("");

  for (let item of list) {
    let sentences = item.split(".");
    for (let s of sentences) {
      let words = s.split(/[\s,-;]/g);
      let node = tree;
      for (let w of words) {
        if (w.length == 0) continue;
        if (!node.hasChild(w)) {
          node.addChild(new Word(w));
        }
        node = node.getChild(w);
        node.increaseCount();
      }
    }
  }

  return tree;
}


collapseTree = (tree) => {
  if (tree instanceof WordOption) {
    collapseTree(tree.word)
  }
  for (let w of tree.childs) {
    collapseTree(w);
    if (w.childs.length == 1 && !(w instanceof WordOption) && !(w.childs[0] instanceof WordOption)) {
      w.childs[0].word = w.word + " " + w.childs[0].word;
      tree.removeChild(w);
      tree.addChild(w.childs[0]);
    }
  }
}

function writeTree(data, key) {
  let tree = createTree(data);
  collapseTree(tree);
  fs.writeFileSync(`./tree_${key}.json`, JSON.stringify(tree.print(), null, 4));
}

writeTree(data.gut, "gut");
writeTree(data.mittel, "mittel");
writeTree(data.schlecht, "schlecht");
