import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np
from sklearn.cluster import KMeans
from sklearn import preprocessing, model_selection
import pandas as pd
from pandas.io.json import json_normalize
import json
import pickle

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

def import_data_and_pickle(name):
    data = json.load(open(name))
    temp = pd.DataFrame.from_dict([data],orient='columns').T
    df = pd.DataFrame()

    for i in range(0,25):
        curr = temp.values[i]
        curr = json_normalize(curr)
        df = df.append(curr,sort=True)

    vals = pd.Series([2,2,3,1,2,1,3,3,1,3,2,3,2,2,3,2,1,1,1,1,1,3,1,2,3])
    df.insert(2, "category",vals.values) 
    #todo flatten
    #df.drop('befunde.Morphologische Korrelation.Sklerose.typ',1,inplace=True)
    #df.drop('befunde.Morphologische Korrelation.Sklerose.bereich',1,inplace=True)
    df.drop('befunde.Aufnahme.Resultat.kammerwand',1,inplace=True)
    df.drop('befunde.Aufnahme.Resultat.attribute',1,inplace=True)
    df.drop('text',1,inplace=True)

    print(df)
    
   # df = df[['id','unauff_Tracer','EF_normal','keine_minder_Tracer','keine_minder_Belast','EF_diff','keine_WandBewStoer']]
    df.fillna(-99999,inplace=True)

    #print(df.head())
    pickle.dump(df,open( "einschaetzung.pickle", "wb" ))

def drop_and_convert_to_numerical():
    df = pickle.load(open( "einschaetzung.pickle", "rb" ))

    def handle_non_numerical_data(df):
        columns = df.columns.values
        for column in columns:
            text_digit_vals = {}
            def convert_to_int(val):
                return text_digit_vals[val]
            
            if df[column].dtype != np.int64 and df[column].dtype != np.float64:
                column_contents = df[column].values.tolist()
                unique_elements = set(column_contents)
                x = 0
                for unique in unique_elements:
                    if unique not in text_digit_vals:
                        text_digit_vals[unique] = x
                        x+=1
                        
                df[column] = list(map(convert_to_int, df[column]))
        return df
    cp = handle_non_numerical_data(df.drop('id',1))
    cp['id'] = df['id']
    #print(cp.head())
    
    pickle.dump(cp,open( "einschaetzung_numerical.pickle", "wb" ))
    
def training():
    df = pickle.load(open("einschaetzung_numerical.pickle", "rb" ))
    #origData = pickle.load(open( "einschaetzung.pickle", "rb" ))
    xdf = df.drop('id',1).astype(float)
    X = np.array(xdf.drop('category',1)) #features
    y= np.array(xdf['category']) #labels
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X,y,test_size=0.1)

    clf = KMeans(n_clusters=3)
    clf.fit(X_train)
    accuracy = clf.score(X_test,y_test) #test with testing data
    print(accuracy)
    dataArr = []
    for i in range(len(X)):
        predict_me = np.array(X[i].astype(float))
        predict_me = predict_me.reshape(-1, len(predict_me))
        prediction = clf.predict(predict_me)
        print(prediction+1)
        dataArr.append(prediction)
    
    
    #pd.DataFrame.to_csv(df,'clustersTest.csv')

import_data_and_pickle('Befunde_regex.json')
drop_and_convert_to_numerical()
training()