import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np
from sklearn.cluster import KMeans
from sklearn import preprocessing, model_selection
import pandas as pd
from pandas.io.json import json_normalize
import json
import pickle

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

def import_data_and_pickle(name):
    data = json.load(open(name))
    temp = pd.DataFrame.from_dict([data],orient='columns').T
    df = pd.DataFrame()

    for i in range(0,temp.shape[0]-1):
        curr = temp.values[i]
        curr = json_normalize(curr)
        df = df.append(curr,sort=True)

    #todo flatten
    df.drop('befunde.Morphologische Korrelation.Sklerose.typ',1,inplace=True)
    df.drop('befunde.Morphologische Korrelation.Sklerose.bereich',1,inplace=True)
    df.drop('befunde.Aufnahme.Resultat.kammerwand',1,inplace=True)
    df.drop('befunde.Aufnahme.Resultat.attribute',1,inplace=True)
    
    #drop unnecessary columns
    df.drop('text',1,inplace=True)
    
    #print(list(df))
    
    #gute sachen true bzw kleine zahlen, schlechte sachen false bzw hohe zahlen
    #________________________gut:__________________________________
    #Die Ruhe- und Belastungsaufnahmen zeigen eine unauffällige Tracerbelegung
    df = df.assign(unauff_Tracer = (df['befunde.Aufnahme.Resultat.typ']=='Tracerbelegung')&(df['befunde.Aufnahme.Resultat.attribut']=='unauffÃ¤llige'))
    #Im Belastungs-EKG keine ischämietypischen Endstreckenveränderungen
    
    #Unauffälliger Koronar-Kalk-Score
    
    #Keine regionale Wandbewegungsstörung
    df = df.assign(keine_WandBewStoer = (df['befunde.regionale WandbewegungsstÃ¶rung']==False))
    #normale EF werte 55-60, wobei <40 notwendiger eingriff
    df = df.assign(EF_normal= df['befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe.prozent']>=41)
    
    
    #______________________schlecht:______________________________
    #Die Ruheaufnahmen zeigen einen Perfusionsdefekt
    #Die Ruheuntersuchung zeigt eine Tracerminderbelegung
    df = df.assign(keine_minder_Tracer = (df['befunde.Aufnahme.Resultat.typ']=='Tracerbelegung')&(df['befunde.Aufnahme.Resultat.minderbelegung']))
    #Im Belastungs-EKG gegen Ende der Belastung ST-Streckensenkungen
    #Die Belastungsaufnahmen zeigen eine Minderbelegung
    df = df.assign(keine_minder_Belast = (df['befunde.Aufnahme.Belastung']&(df['befunde.Aufnahme.Resultat.minderbelegung'])))

    #Kein Nachweis von Narbengewebe im linksventrikulären Myokard
    
    #gr. unterschied werte
    df = df.assign(EF_diff = abs(df['befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe nach Belastung.prozent']-df['befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe.prozent'])<=10)
    df['EF_diff'].fillna(0,inplace=True)
    #drop all used columns
    #df.drop({'befunde.Aufnahme.Belastung','befunde.Aufnahme.Resultat.minderbelegung','befunde.Aufnahme.Resultat.typ','befunde.Aufnahme.Resultat.attribut','befunde.linksventrikulÃ¤re Auswurffraktion','befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe nach Belastung.prozent','befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe.prozent','befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe nach Belastung.mehrAls','befunde.linksventrikulÃ¤re Auswurffraktion.Ruhe.mehrAls','befunde.linksventrikulÃ¤re Auswurffraktion.unter Belastung.mehrAls','befunde.linksventrikulÃ¤re Auswurffraktion.unter Belastung.prozent'},1,inplace=True)
    
    df = df[['id','unauff_Tracer','EF_normal','keine_minder_Tracer','keine_minder_Belast','EF_diff','keine_WandBewStoer']]
    df.fillna(-99999,inplace=True)

    print(df.head())
    pickle.dump(df,open( "dataTable.pickle", "wb" ))

def drop_and_convert_to_numerical():
    df = pickle.load(open( "dataTable.pickle", "rb" ))

    def handle_non_numerical_data(df):
        columns = df.columns.values
        for column in columns:
            text_digit_vals = {}
            def convert_to_int(val):
                return text_digit_vals[val]
            
            if df[column].dtype != np.int64 and df[column].dtype != np.float64:
                column_contents = df[column].values.tolist()
                unique_elements = set(column_contents)
                x = 0
                for unique in unique_elements:
                    if unique not in text_digit_vals:
                        text_digit_vals[unique] = x
                        x+=1
                        
                df[column] = list(map(convert_to_int, df[column]))
        return df
    cp = handle_non_numerical_data(df.drop('id',1))
    cp['id'] = df['id']
    #print(cp.head())
    
    pickle.dump(cp,open( "dataTable_numerical.pickle", "wb" ))
    
def training():
    df = pickle.load(open("dataTable_numerical.pickle", "rb" ))
    origData = pickle.load(open( "dataTable.pickle", "rb" ))
    X = np.array(df.drop('id',1).astype(float))

    clf = KMeans(n_clusters=2)
    clf.fit(X)
    dataArr = []
    for i in range(len(X)):
        predict_me = np.array(X[i].astype(float))
        predict_me = predict_me.reshape(-1, len(predict_me))
        prediction = clf.predict(predict_me)
        dataArr.append(prediction)
    
    df = origData[['id','unauff_Tracer','EF_normal','keine_minder_Tracer','keine_minder_Belast','EF_diff','keine_WandBewStoer']]
    df = df.assign(cluster = dataArr)
    df = df[['id','cluster','unauff_Tracer','EF_normal','keine_minder_Tracer','keine_minder_Belast','EF_diff','keine_WandBewStoer']]
    print(df.head())
    pd.DataFrame.to_csv(df,'clusters.csv')

import_data_and_pickle('Befunde_regex.json')
#drop_and_convert_to_numerical()
#training()