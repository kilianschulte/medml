# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 10:59:50 2019

@author: wiifr
"""

import numpy as np
from sklearn import preprocessing, model_selection, neighbors
import pandas as pd
from math import sqrt

df = pd.read_csv('breast-cancer-wisconsin.data.txt')
df.replace('?',-99999,inplace=True) # replace placeholders
df.drop(['id'],1,inplace=True) # drop ID col, not relevant

X = np.array(df.drop(['class'],1)) # features
y = np.array(df['class']) #labels/classes

X_train, X_test, y_train, y_test = model_selection.train_test_split(X,y,test_size=0.2)
clf = neighbors.KNeighborsClassifier()
clf.fit(X_train,y_train) #train data

accuracy = clf.score(X_test,y_test) #test data
print(accuracy)

example_measures = np.array([[4,2,1,2,1,1,3,2,1],[4,2,1,1,1,2,3,2,1]])
example_measures = example_measures.reshape(len(example_measures), -1)

prediction = clf.predict(example_measures)
print(prediction)