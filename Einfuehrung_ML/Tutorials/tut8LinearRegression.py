# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 21:46:09 2019

@author: wiifr
"""

from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style

style.use('fivethirtyeight')

xs = np.array([1,2,3,4,5,6], dtype=np.float64)
ys = np.array([5,4,6,5,6,7], dtype=np.float64)

def best_fit_slope(xs,ys):
    mean_first = mean(xs)*mean(ys)
    mean_second = mean(xs*ys)
    mean_third = mean(xs)**2
    mean_fourth = mean(xs**2)
    m = (mean_first-mean_second)/(mean_third-mean_fourth)
    #m = (mean(x)*mean(y) - mean(x*y))/(mean(x)^2-mean(x^2))
    b = mean(ys)-m*mean(xs)
    #b = mean(y)-m*mean(x)
    return m,b

m ,b = best_fit_slope(xs,ys)

predict_x = 8
predict_y = (m*predict_x+b)

plt.scatter(xs,ys)
plt.scatter(predict_x,predict_y)
ys = m*xs+b
plt.plot(xs,ys)
plt.show()