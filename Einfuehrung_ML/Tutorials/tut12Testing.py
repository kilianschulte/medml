from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
import random

style.use('fivethirtyeight')

xs = np.array([1,2,3,4,5,6], dtype=np.float64)
ys = np.array([5,4,6,5,6,7], dtype=np.float64)

def create_dataset(hm, variance, step=2, correlation=False): #hm= how many dp
    val = 1
    ys = []
    for i in range(hm): #create new random data
        y = val + random.randrange(-variance,variance)
        ys.append(y)
        if correlation == 'pos':
            val+=step
        elif correlation == 'neg':
            val-=step
            
    xs = [i for i in range(len(ys))]
        
    return np.array(xs,dtype=np.float64), np.array(ys, dtype=np.float64)
    
def best_fit_slope(xs,ys):
    mean_first = mean(xs)*mean(ys)
    mean_second = mean(xs*ys)
    mean_third = mean(xs)**2
    mean_fourth = mean(xs**2)
    m = (mean_first-mean_second)/(mean_third-mean_fourth)
    #m = (mean(x)*mean(y) - mean(x*y))/(mean(x)^2-mean(x^2))
    b = mean(ys)-m*mean(xs)
    #b = mean(y)-m*mean(x)
    return m,b

def rsquare(newys,ys):
    meanys = [mean(ys) for _ in ys]
    err = 1 - (squared_error(ys,newys)/squared_error(ys,meanys))
    return err

def squared_error(ys_orig, ys_line):
    return sum((ys_line-ys_orig)**2)

xs, ys = create_dataset(40, 40,2,correlation='pos')
#low variance -> high r

m ,b = best_fit_slope(xs,ys)

predict_x = 40
predict_y = (m*predict_x+b)

plt.scatter(xs,ys)
plt.scatter(predict_x,predict_y)
newys = [(m*x+b) for x in xs]
print(rsquare(newys,ys))
plt.plot(xs,newys)
plt.show()