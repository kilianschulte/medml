import pandas as pd
import quandl, math, datetime
import numpy as np
from sklearn import preprocessing, svm, model_selection
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from matplotlib import style

style.use('ggplot')
quandl.ApiConfig.api_key = "ydjcA9REvSR2yDx35exa"


df = quandl.get('WIKI/GOOGL')
df = df[['Adj. Open', 'Adj. High', 'Adj. Low', 'Adj. Close', 'Adj. Volume']]
df['HL_PCT'] = (df['Adj. High'] - df['Adj. Low'])/df['Adj. Close']*100.0
df['PCT_change'] = (df['Adj. Close']-df['Adj. Open'])/df['Adj. Open']*100.0

df = df[['Adj. Close', 'HL_PCT', 'PCT_change', 'Adj. Volume']]

forecast_col = 'Adj. Close'
df.fillna(-99999, inplace=True)

forecast_out = int(math.ceil(0.1*len(df))) #how much shift

df['label'] = df[forecast_col].shift(-forecast_out)

X = np.array(df.drop(['label'],1)) #features
X = preprocessing.scale(X)
X_lately = X[-forecast_out:]
X = X[:-forecast_out]

df.dropna(inplace=True)
y = np.array(df['label']) #labels

X_train, X_test, y_train, y_test = model_selection.train_test_split(X,y,test_size=0.2)
#20 percent of data is used as training data

#find classifiers
clf = LinearRegression() # init classifiers with Linear Regression
clf.fit(X_train, y_train) #train with training data
accuracy = clf.score(X_test, y_test) #test with testing data

forecast_set = clf.predict(X_lately)  #prediction for next days

#print
print('forecast set: ')
print( forecast_set)
print('accuracy: ',accuracy)
print('days forecasted: ',forecast_out)
df['Forecast'] = np.nan

last_date = df.iloc[-1].name # get the last date name
last_unix = last_date.timestamp() 
one_day = 86400 #sec/day
next_unix = last_unix + one_day #next unix is last one + 1 day

for i in forecast_set:
    next_date = datetime.datetime.fromtimestamp(next_unix)
    next_unix += one_day
    df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)] + [i]
    #add (nrcolumns) nans and the new forecasted value to the df
    
df['Adj. Close'].plot()
df['Forecast'].plot()
plt.legend(loc=4)
plt.xlabel('Date')
plt.ylabel('Price')
plt.show()